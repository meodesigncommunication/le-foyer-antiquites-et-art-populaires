<?php

class ImageZoom_LicenseForm {

    var $data = array();

    function __construct( $data = array() ) {
        $data['nonce'] = $data['license'] . '_nonce';
        $data['activate'] = $data['license'] . '_activate';
        $data['deactivate'] = $data['license'] . '_deactivate';
        $this->data = $data;
        

        if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
            include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
        }
    }


    /**
     * License form 
     */
    function license_page() {
        $license 	= get_option( $this->data['license_key'] );
        $status 	= get_option( $this->data['license_status'] );
        $beta       = get_option( $this->data['license_beta'] );
        ?>
            <form method="post">

                <?php settings_fields( $this->data['license'] ); ?>

                <table class="form-table">
                    <tbody>
                        <tr valign="top">
                            <th scope="row" valign="top">
                                <?php _e('License Key'); ?>
                            </th>
                            <td>
                                <?php wp_nonce_field( $this->data['nonce'], $this->data['nonce'] ); ?>
                                <input id="<?php echo $this->data['license_key']; ?>" name="<?php echo $this->data['license_key']; ?>" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
                                <label class="description" for="<?php echo $this->data['license_key']; ?>"><?php _e('Enter your license key'); ?></label>
                            </td>
                        </tr>
                        <?php if( false !== $license ) { ?>
                            <tr valign="top">
                                <th scope="row" valign="top">
                                    <?php _e('Activate License'); ?>
                                </th>
                                <td>
                                    <?php if( $status !== false && $status == 'valid' ) { ?>
                                        <span style="color:green;"><?php _e('active'); ?></span>
                                        <input type="submit" class="button-secondary" name="<?php echo $this->data['deactivate']; ?>" value="<?php _e('Deactivate License'); ?>"/>
                                    <?php } else { ?>
                                        <input type="submit" class="button-secondary" name="<?php echo $this->data['activate']; ?>" value="<?php _e('Activate License'); ?>"/>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th scope="row" valign="top">
                                    <?php _e('Get pre-release updates'); ?>
                                </th>
                                <td>
                                <input type="checkbox"  name="<?php echo $this->data['license_beta']; ?>" value="1" <?php if ( $beta == '1' ) echo 'checked="checked"'; ?> />

                                    You will receive pre-release update notifications. Pre-release updates do not install automatically, you still have to option to ignore the update notifications.
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary">Save changes</button>

            </form>
        <?php
    }

    function save_changes() {
        $d = $this->data;

        if( ! check_admin_referer( $d['nonce'], $d['nonce'] ) )
            return; 

        $old = get_option( $d['license_key'] );
        $new = $_POST[$d['license_key']];
        if( $old != $new ) {
            update_option( $d['license_key'], $new );
            // new license has been entered, so must reactivate
            delete_option( $d['license_status'] ); 
        }

        $license_beta = isset($_POST[$d['license_beta']]) ? '1' : '0';
        update_option( $d['license_beta'], $license_beta);
    }


    /**
     * Activate the license
     */
    function activate_deactivate_license() {

        $d = $this->data;

        $action = false;
        if ( isset($_POST[$d['activate']])) $action = 'activate_license';
        if ( isset($_POST[$d['deactivate']])) $action = 'deactivate_license';

        if ( $action === false )
            return;


        if( ! check_admin_referer( $d['nonce'], $d['nonce'] ) )
            return; 

        $license = trim( get_option( $d['license_key'] ) );


        $api_params = array(
            'edd_action' => $action,
            'license'    => $license,
            'item_name'  => urlencode( $d['item_name'] ), 
            'url'        => home_url()
        );

        $response = wp_remote_post( $d['store_url'], array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

        if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

            if ( is_wp_error( $response ) ) {
                $message = $response->get_error_message();
            } else {
                $message = __( 'An error occurred, please try again.' );
            }

            return $message;

        } else {

            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

            if ( false === $license_data->success ) {

                switch( $license_data->error ) {

                    case 'expired' :

                        $message = sprintf(
                            __( 'Your license key expired on %s.' ),
                            date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
                        );
                        break;

                    case 'revoked' :

                        $message = __( 'Your license key has been disabled.' );
                        break;

                    case 'missing' :

                        $message = __( 'Invalid license.' );
                        break;

                    case 'invalid' :
                    case 'site_inactive' :

                        $message = __( 'Your license is not active for this URL.' );
                        break;

                    case 'item_name_mismatch' :

                        $message = sprintf( __( 'This appears to be an invalid license key for %s.' ), $d['item_name'] );
                        break;

                    case 'no_activations_left':

                        $message = __( 'Your license key has reached its activation limit.' );
                        break;

                    default :

                        $message = __( 'An error occurred, please try again.' );
                        break;
                }

                return $message;

            }

        }

        if ( $action == 'activate_license' ) {
            update_option( $this->data['license_status'], $license_data->license );

            $message = __('The license was successfully activated for this website.');
        } elseif ( $license_data->license == 'deactivated' ) {
            delete_option( $this->data['license_status'] );
            $message = __('The license was successfully deactivated for this website.');
        }

        return $message;


    }


}
