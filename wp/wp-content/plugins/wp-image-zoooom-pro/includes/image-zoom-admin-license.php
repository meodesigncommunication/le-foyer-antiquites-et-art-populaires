<?php $main = ImageZoooomPRO(); ?>


<h2>WP Image Zoom PRO by <img src="<?php echo site_url(); ?>/wp-content/plugins/wp-image-zoooom-pro/assets/images/silkypress_logo.png" /> <a href="https://www.silkypress.com/" target="_blank">SilkyPress.com</a></h2>

<div class="wrap">

<h3 class="nav-tab-wrapper woo-nav-tab-wrapper">
    <a href="?page=zoooom_settings&tab=general" class="nav-tab"><?php echo __('General Settings', 'zoooom'); ?></a>
    <a href="?page=zoooom_settings&tab=settings" class="nav-tab"><?php echo __('Zoom Settings'); ?></a>
    <a href="?page=zoooom_settings&tab=license" class="nav-tab nav-tab-active"><?php echo __('License Key'); ?></a>
</h3>

<div class="panel panel-default">
    <div class="panel-body">
    <div class="row">
    <div class="col-lg-12">


<?php
    require_once('edd/edd-plugin.php');

    $license_data = array(
        'store_url'     => $main->plugin_server, 
        'item_name'     => $main->plugin_name, 
        'author'        => $main->author, 
        'version'       => $main->version,
        'main_file'     => $main->file,
        'prefix'        => 'iz_',
        'license'       => 'image_zoom_license',
        'license_key'   => 'image_zoom_license_key',
        'license_status' => 'image_zoom_license_status',
        'license_beta'  => 'image_zoom_license_beta',
    );

    $edd = new ImageZoom_LicenseForm( $license_data );

    $message = false;
    if ( ! empty( $_POST ) ) {
        $edd->save_changes();
        $message = $edd->activate_deactivate_license();
    }


    if ( $message != false ) {
        $alert_type = ( strstr($message, 'success')) ? 'success' : 'danger';
    echo '<div class="alert alert-'.$alert_type.'">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <b>'.$message.'</b>
    </div>';
    }


    $edd->license_page();

?>
    </div>
    </div>
    </div>
</div>
</div>

<?php include_once('right_columns.php'); ?>
