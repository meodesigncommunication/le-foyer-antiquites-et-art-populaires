<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * ImageZoooom_Admin 
 */
class ImageZoooom_Admin_PRO {

    public $messages = array();
    private $tab = 'general';
    private $main = '';
    private $asset_url = '';

    /**
     * Constructor
     */
    public function __construct() {

        if ( ! function_exists( 'is_plugin_active' ) ) {
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        } 

        $free_version = 'wp-image-zoooom/image-zoooom.php';
        if ( is_plugin_active( $free_version ) ) {
            deactivate_plugins( $free_version );
            $pro_version = 'wp-image-zoooom-pro/image-zoooom-pro.php';
            activate_plugins( $pro_version );
            return false;
        }

        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
        add_action( 'admin_head', array( $this, 'iz_add_tinymce_button' ) );

        $this->main = ImageZoooomPRO();
        $this->asset_url = $this->main->plugins_url('/assets/') . '/';

        $this->edd_updater();
    }

    /**
     * Add menu items
     */
    public function admin_menu() {
        add_menu_page(
            __( 'WP Image Zoom', 'zoooom' ),
            __( 'WP Image Zoom', 'zoooom' ),
            'administrator',
            'zoooom_settings',
            array( $this, 'admin_settings_page' ),
            $this->asset_url. 'images/icon.svg'
        );
    }

    /**
     * Load the javascript and css scripts
     */
    public function admin_enqueue_scripts( $hook ) {
        if ( $hook != 'toplevel_page_zoooom_settings' )
            return false;

        $prefix = '.min';
        if ( $this->main->testing == true ) $prefix = '';

        $url = $this->asset_url;
        $version = $this->main->version;

        // Register and enqueue the javascript files
        wp_enqueue_script( 'iz-bootstrap', $url .'js/bootstrap.3.2.0.min.js', array( 'jquery' ), $version, true  );
        wp_enqueue_script( 'iz-image_zoooom', $url .'js/jquery.image_zoom'.$prefix.'.js', array( 'jquery' ), $version, true );
        if ( !isset($_GET['tab']) || $_GET['tab'] == 'settings' ) {
            wp_enqueue_script( 'iz-zoooom-settings', $url .'js/image_zoom.settings.js', array( 'iz-image_zoooom' ), $version, true );
        }

        // Register the css files
        wp_enqueue_style( 'iz-bootstrap', $url.'css/bootstrap.min.css', array(), $version );
        wp_enqueue_style( 'iz-zoooom', $url .'css/style'.$prefix.'.css', array(), $version );
    }

    /**
     * Build an array with settings that will be used in the form
     * @access public
     */
    public function get_settings( $id  = '' ) {
        $settings = array(
            'lensShape' => array(
                'label' => __('Lens Shape', 'zoooom'),
                'values' => array(
                    'none' => array('icon-lens_shape_none', __('No Lens', 'zoooom')),
                    'round' => array('icon-lens_shape_circle', __('Circle Lens', 'zoooom')),
                    'square' => array('icon-lens_shape_square', __('Square Lens', 'zoooom')),
                    'zoom_window' => array('icon-type_zoom_window', __('With Zoom Window', 'zoooom')),
                ),
                'value' => 'zoom_window',
                'input_form' => 'buttons',
                'buttons' => 'i',
            ),
            'cursorType' => array(
                'label' => __('Cursor Type', 'zoooom'),
                'values' => array(
                    'default' => array('icon-cursor_type_default', __('Default', 'zoooom' ) ),
                    'pointer' => array('icon-cursor_type_pointer', __('Pointer', 'zoooom' ) ),
                    'crosshair' => array('icon-cursor_type_crosshair', __('Crosshair', 'zoooom' ) ),
                    'zoom-in' => array('icon-zoom-in', __('Zoom', 'zoooom' ) ),
                ),
                'value' => 'default',
                'input_form' => 'buttons',
                'buttons' => 'i',
            ),
            'zwEasing' => array(
                'label' => __('Animation Easing Effect', 'zoooom' ),
                'value' => 12,
                'description' => __('A number between 0 and 200 to represent the degree of the Animation Easing Effect', 'zoooom' ),
                'input_form' => 'input_text',
            ),
            'onClick' => array(
                'label' => __('Enable the zoom on ...', 'zoooom'),
                'values' => array(
                    'false' => 'mouse hover',
                    'true' => 'mouse click',
                ),
                'value' => 'false',
                'input_form' => 'radio',
            ),
            'ratio' => array(
                'label' => __('Zoom Level', 'zoooom'),
                'values' => array(
                    'default' => array( 'icon-zoom_level_default', __('Default', 'zoooom') ),
                    '1.5' => array( 'icon-zoom_level_15', __('1,5 times', 'zoooom') ),
                    '2' => array( 'icon-zoom_level_2', __('2 times', 'zoooom') ),
                    '2.5' => array( 'icon-zoom_level_25', __('2,5 times', 'zoooom') ),
                    '3' => array( 'icon-zoom_level_3', __('3 times', 'zoooom') ),
                ),
                'value' => 'default',
                'description' => __('By default the zoom level is calculated automatically from the full size of the uploaded image', 'zoooom'),
                'input_form' => 'buttons',
                'buttons' => 'i',
            ),

            'lensSize' => array(
                'label' => __('Lens Size', 'zoooom' ),
                'post_input' => 'px',
                'value' => 200,
                'description' => __('For Circle Lens it means the diameters, for Square Lens it means the width', 'zoooom' ),
                'input_form' => 'input_text',
            ),
            'lensColour' => array(
                'label' => __('Lens Color', 'zoooom' ),
                'value' => '#ffffff',
                'input_form' => 'input_color',
                'description' => __('Only for Zoom Window. Only when Tint is not used', 'zoooom' ),
            ),
            'lensOverlay' => array(
                'label' => __('Show as Grid', 'zoooom' ),
                'value' => false,
                'input_form' => 'checkbox',
                'description' => __('Only for Zoom Window. Only when Tint is not used. Change the color with the Background Color setting', 'zoooom' ),
            ),
            'borderThickness' => array(
                'label' => __('Border Thickness', 'zoooom' ),
                'post_input' => 'px',
                'value' => 1,
                'input_form' => 'input_text',
            ),
            'borderColor' => array(
                'label' => __('Border Color', 'zoooom' ),
                'value' => '#ffffff',
                'input_form' => 'input_color',
            ),
            'lensFade' => array(
                'label' => __('Fade Time', 'zoooom' ),
                'post_input' => 'sec',
                'value' => 1,
                'description' => __('The amount of time it takes for the Lens to slowly appear or dissapear', 'zoooom'),
                'input_form' => 'input_text',
            ),
            'tint' => array(
                'label' => __('Tint', 'zoooom'),
                'value' => false,
                'description' => __('A color that will layed on top the of non-magnified image in order to emphasize the lens', 'zoooom'),
                'input_form' => 'checkbox',
            ),
            'tintColor' =>array(
                'label' => __('Tint Color', 'zoooom'),
                'value' => '#ffffff',
                'input_form' => 'input_color',
            ),
            'tintOpacity' => array(
                'label' => __('Tint Opacity', 'zoooom'),
                'value' => '0.5',
                'post_input' => '%',
                'input_form' => 'input_text',
            ),
            'zwWidth' => array(
                'label' => __('Zoom Window Width', 'zoooom'),
                'post_input' => 'px',
                'value' => 400,
                'input_form' => 'input_text',
            ),
            'zwHeight' => array(
                'label' => __('Zoom Window Height', 'zoooom'),
                'post_input' => 'px',
                'value' => 360,
                'input_form' => 'input_text',
            ),
            'zwResponsive' => array(
                'label' => __('Responsive', 'zoooom'),
                'description' => __('The Zoom Window Width will adapt to the browser\'s size. Works only on the frontend, not on this demo.', 'zoooom'),
                'input_form' => 'checkbox',
                'value' => true,
            ),
            'zwResponsiveThreshold' => array(
                'label' => __('Responsive Threshold', 'zoooom'),
                'description' => __('The zoom type will authomatically switch to Inner Zoom once the browser\'s width becomes less than the threshold. Works only on the frontend, not on this demo.', 'zoooom'),
                'post_input' => 'px',
                'value' => 800,
                'input_form' => 'input_text',
            ),
            'zwPositioning' => array(
                'label' => __('Positioning', 'zoooom'),
                'values' => array(
                    'right_top' => array('icon-type_zoom_window_right_top', __('Right Top', 'zoooom')),
                    'right_bottom' => array('icon-type_zoom_window_right_bottom', __('Right Bottom', 'zoooom')),
                    'right_center' => array('icon-type_zoom_window_right_center', __('Right Center', 'zoooom')),
                    'left_top' => array('icon-type_zoom_window_left_top', __('Left Top', 'zoooom')),
                    'left_bottom' => array('icon-type_zoom_window_left_bottom', __('Left Bottom', 'zoooom')),
                    'left_center' => array('icon-type_zoom_window_left_center', __('Left Center', 'zoooom')),
                ),
                'value' => 'right_top',
                'description' => __('Feature available only in the PRO Version', 'zoooom'),
                'disabled' => true,
                'input_form' => 'buttons',
                'buttons' => 'i',
            ),
            'zwPadding' => array(
                'label' => __('Distance from the Main Image', 'zoooom'),
                'post_input' => 'px',
                'value' => 10,
                'input_form' => 'input_text',
            ),
            'zwBorderThickness' => array(
                'label' => __('Border Thickness', 'zoooom'),
                'post_input' => 'px',
                'value' => 4,
                'input_form' => 'input_text',
            ),
            'zwShadow' => array(
                'label' => __('Shadow Thickness', 'zoooom'),
                'post_input' => 'px',
                'value' => 4,
                'input_form' => 'input_text',
                'description' => __('Use 0px to remove the shadow', 'zoooom'),
            ),
            'zwBorderColor' => array(
                'label' => __('Border Color', 'zoooom'),
                'value' => '#888888',
                'input_form' => 'input_color',
            ),
            'zwBorderRadius' => array(
                'label' => __('Rounded Corners', 'zoooom'),
                'post_input' => 'px',
                'value' => 0,
                'input_form' => 'input_text',
            ),
            'mousewheelZoom' => array(
                'label' => __('Mousewheel Zoom', 'zoooom'),
                'value' => true,
                'description' => __('When using the mousewheel, the zoomed level of the image will change', 'zoooom'),
                'input_form' => 'checkbox',
            ),
            'zwFade' => array(
                'label' => __('Fade Time', 'zoooom'),
                'post_input' => 'sec',
                'value' => 0,
                'description' => __('The amount of time it takes for the Zoom Window to slowly appear or disappear', 'zoooom'),
                'input_form' => 'input_text',
            ),            
            'customText' => array(
                'label' => __('Text on the image', 'zoooom'),
                'value' => __('', 'zoooom'),
                'description' => __('Feature available only in the PRO Version', 'zoooom'),
                'input_form' => 'input_text',
                'disabled' => true,
            ),
            'customTextSize' => array(
                'label' => __('Text Size', 'zoooom'),
                'post_input' => 'px',
                'value' => 12,
                'description' => __('Feature available only in the PRO Version', 'zoooom'),
                'input_form' => 'input_text',
                'disabled' => true,
            ),
            'customTextColor' => array(
                'label' => __('Text Color', 'zoooom'),
                'value' => '#cccccc',
                'description' => __('Feature available only in the PRO Version', 'zoooom'),
                'input_form' => 'input_color',
                'disabled' => true,
            ),            
            'customTextAlign' => array(
                'label' => __('Text Align', 'zoooom'),
                'values' => array(
                    'top_left' => array('icon-text_align_top_left', __('Top Left', 'zoooom' ) ),
                    'top_center' => array('icon-text_align_top_center', __('Top Center', 'zoooom' ) ),
                    'top_right' => array('icon-text_align_top_right', __('Top Right', 'zoooom' ) ),
                    'bottom_left' => array('icon-text_align_bottom_left', __('Bottom Left', 'zoooom' ) ),
                    'bottom_center' => array('icon-text_align_bottom_center', __('Bottom Center', 'zoooom' ) ),
                    'bottom_right' => array('icon-text_align_bottom_right', __('Bottom Right', 'zoooom' ) ),
                ),
                'value' => 'bottom_right',
                'input_form' => 'buttons',
                'buttons' => 'i',
            ),
            'enable_woocommerce' => array(
                'label' => __('Enable the zoom on WooCommerce products', 'zoooom'),
                'value' => true,
                'input_form' => 'checkbox',
            ),
            'exchange_thumbnails' => array(
                'label' => __('Exchange the thumbnail with main image on WooCommerce products', 'zoooom'),
                'value' => true,
                'input_form' => 'checkbox',
                'description' => __('On a WooCommerce gallery, when clicking on a thumbnail, not only the main image will be replaced with the thumbnail\'s image, but also the thumbnail will be replaced with the main image', 'zoooom'),
            ),
            'enable_mobile' => array(
                'label' => __('Enable the zoom on mobile devices', 'zoooom'),
                'value' => false,
                'input_form' => 'checkbox',
            ),
            'woo_variations' => array(
                'label' => __('Enable on WooCommerce variation products ', 'zoooom'),
                'value' => false,
                'input_form' => 'checkbox',
            ),
            'woo_cat' => array(
                'label' => __('Enable the zoom on WooCommerce category pages', 'zoooom'),
                'value' => false,
                'input_form' => 'checkbox',
            ),
            'force_woocommerce' => array(
                'label' => __('Force it to work on WooCommerce', 'zoooom'),
                'value' => true,
                'input_form' => 'checkbox',
            ),
            'remove_lightbox_thumbnails' => array(
                'label' => __('Remove the Lightbox on WooCommerce product thumbnail images', 'zoooom'),
                'value' => true,
                'input_form' => 'checkbox',
                'description' => __('Some themes implement a Lightbox for WooCommerce galleris that opens on click. Enabling this checkbox will remove the Lightbox on thumbnail images and leave it only on the main image'),
            ),
            'remove_lightbox' => array(
                'label' => __('Remove the Lightbox on WooCommerce products', 'zoooom'),
                'value' => true,
                'input_form' => 'checkbox',
                'description' => __('Some themes implement a Lightbox that opens on click on the image. Enabling this checkbox will remove the Lightbox'),
            ),

            'force_attachments' => array(
                'label' => __('Enable on attachments pages', 'zoooom'),
                'value' => false,
                'input_form' => 'checkbox',
            ),
            'custom_class' => array(
                'label' => __('Apply zoom on this particular image(s)', 'zoooom'),
                'value' => '',
                'input_form' => 'input_text',
                'description' => __('CSS style selector(s) for identifying the image(s) on which to apply the zoom.', 'zoooom' ),
            ),
            'flexslider' => array(
                'label' => __('FlexSlider container class', 'zoooom'),
                'value' => '',
                'input_form' => 'input_text',
                'description' => __('If the images are in a Woo FlexSlider gallery, then type in here the class of the div containing the FlexSlider gallery', 'zoooom' ),
            ),
            'huge_it_gallery' => array(
                'label' => __('<a href="https://wordpress.org/plugins/gallery-images/" target="_blank">Huge IT Gallery</a> id', 'zoooom'),
                'value' => '',
                'input_form' => 'input_text',
                'description' => __('In order to apply to more galleries, enter the respective ids separated by comma', 'zoooom' ),
            ),
            'enable_photo_gallery' => array(
                'label' => __('Enable on <a href="https://wordpress.org/plugins/photo-gallery/" target="_blank">Photo Gallery</a> Lightbox', 'zoooom'),
                'value' => '',
                'input_form' => 'checkbox',
                'description' => __('The zoom will be enable only on the image opened in Lightbox, not on the gallery\'s thumbnails', 'zoooom' ),
            ),
            'enable_fancybox' => array(
                'label' => __('Enable inside <a href="http://fancyapps.com/fancybox/" target="_blank">fancyBox</a> Lightbox', 'zoooom'),
                'value' => false,
                'input_form' => 'checkbox',
            ),
            'enable_jetpack_carousel' => array(
                'label' => __('Enable inside <a href="https://jetpack.com/support/carousel/" target="_blank">Jetpack Carousel</a> lightbox', 'zoooom'),
                'value' => false,
                'input_form' => 'checkbox',
            ),


        );

        if ( get_option( 'image_zoom_license_status' ) == 'valid' ) {
            foreach ( array('customText', 'customTextSize', 'customTextColor', 'zwPositioning') as $_field ) {
                $settings[$_field]['disabled'] = false;
                $settings[$_field]['description'] = '';
            }
        }

        if ( isset( $settings[$id] ) ) {
            $settings[$id]['name'] = $id;
            return $settings[$id];
        } elseif ( empty( $id ) ) {
            return $settings;
        }
        return false;
    }


    /**
     * Output the admin page
     * @access public
     */
    public function admin_settings_page() {

        if ( ! isset($_GET['tab'] ) ) $_GET['tab'] = 'settings';

        switch ( $_GET['tab'] ) {
            case 'general' : 
                if ( ! empty( $_POST ) ) {
                    check_admin_referer('iz_general');
                    $new_settings = $this->validate_general( $_POST );
                    update_option( 'zoooom_general', $new_settings );
                    self::add_message( 'success', '<b>'.__('Your settings have been saved.', 'zoooom') . '</b>' );
                }

                $template = $this->main->plugin_dir_path() . "/includes/image-zoom-admin-general.php";
                load_template( $template );

                $this->tab = 'general';

                break;

            case 'license' :

                $template = $this->main->plugin_dir_path() . "/includes/image-zoom-admin-license.php";
                load_template( $template );

                break;

            default : 
                if ( ! empty( $_POST ) ) {
                    check_admin_referer('iz_template');
                    $new_settings = $this->validate_settings( $_POST );
                    $new_settings_js = $this->generate_js_settings( $new_settings );
                    update_option( 'zoooom_settings', $new_settings );
                    update_option( 'zoooom_settings_js', $new_settings_js );
                    self::add_message( 'success', '<b>'.__('Your settings have been saved.', 'zoooom') . '</b>' );
                }

                $template = $this->main->plugin_dir_path() . "/includes/image-zoom-admin-template.php";
                load_template( $template );

                $this->tab = 'settings';
                break;
        }
    }

    /**
     * Build the jquery.image_zoom.js options and save them directly in the database
     * @access private
     */
    private function generate_js_settings( $settings ) {
        $options = array();
        switch ( $settings['lensShape'] ) {
            case 'none' : 
                $options[] = 'zoomType : "inner"';
                $options[] = 'cursor: "'.$settings['cursorType'].'"';
                $options[] = 'easingAmount: '.$settings['zwEasing'];
                break;
            case 'square' :
            case 'round' :
                $options[] = 'lensShape     : "' .$settings['lensShape'].'"';
                $options[] = 'zoomType      : "lens"';
                $options[] = 'lensSize      : "' .$settings['lensSize'].'"';
                $options[] = 'borderSize    : "' .$settings['borderThickness'].'"'; 
                $options[] = 'borderColour  : "' .$settings['borderColor'].'"';
                $options[] = 'cursor        : "' .$settings['cursorType'].'"';
                $options[] = 'lensFadeIn    : "' .$settings['lensFade'].'"';
                $options[] = 'lensFadeOut   : "' .$settings['lensFade'].'"';
                if ( $settings['tint'] == true ) {
                    $options[] = 'tint     : true';
                    $options[] = 'tintColour:  "' . $settings['tintColor'] . '"';
                    $options[] = 'tintOpacity:  "' . $settings['tintOpacity'] . '"';
                }
 
                break;
            case 'square' :
                break;
            case 'zoom_window' :
                $zwPositioning = 1;
                if ( $settings['zwPositioning'] == 'right_bottom' ) $zwPositioning  = 3;
                if ( $settings['zwPositioning'] == 'right_center' ) $zwPositioning  = 2;
                if ( $settings['zwPositioning'] == 'left_top' ) $zwPositioning  = 11;
                if ( $settings['zwPositioning'] == 'left_bottom' ) $zwPositioning  = 9;
                if ( $settings['zwPositioning'] == 'left_center' ) $zwPositioning  = 10;
                if ( $settings['lensOverlay'] == true ) $settings['lensOverlay'] = $this->main->plugins_url() . '/assets/images/lens-overlay-1.png';
                $settings['zwResponsive'] = ( $settings['zwResponsive'] ) ? 'true' : 'false';
                $settings['lensOverlay'] = ( $settings['lensOverlay'] ) ? 'true' : 'false';
                $settings['mousewheelZoom'] = ( $settings['mousewheelZoom'] ) ? 'true' : 'false';

               $options[] = 'lensShape       : "square"';
               $options[] = 'lensSize        : "' .$settings['lensSize'].'"'; 
               $options[] = 'lensColour      : "' .$settings['lensColour'].'"'; 
               $options[] = 'lensOverlay     : ' .$settings['lensOverlay']; 
               $options[] = 'lensBorderSize  : "' .$settings['borderThickness'].'"'; 
               $options[] = 'lensBorderColour: "' .$settings['borderColor'].'"'; 
               $options[] = 'borderRadius    : "' .$settings['zwBorderRadius'].'"'; 
               $options[] = 'cursor          : "' .$settings['cursorType'].'"';
               $options[] = 'zoomWindowWidth : "' .$settings['zwWidth'].'"';
               $options[] = 'zoomWindowHeight: "' .$settings['zwHeight'].'"';
               $options[] = 'responsive      : ' .$settings['zwResponsive'];
               $options[] = 'responsiveThreshold : "' .$settings['zwResponsiveThreshold'].'"';
               $options[] = 'zoomWindowOffsetx: "' .$settings['zwPadding'].'"';
               $options[] = 'borderSize      : "' .$settings['zwBorderThickness'].'"';
               $options[] = 'borderColour    : "' .$settings['zwBorderColor'].'"';
               $options[] = 'zoomWindowShadow : "' .$settings['zwShadow'].'"';
               $options[] = 'lensFadeIn      : "' .$settings['lensFade'].'"';
               $options[] = 'lensFadeOut     : "' .$settings['lensFade'].'"';
               $options[] = 'zoomWindowFadeIn  :"' .$settings['zwFade'].'"';
               $options[] = 'zoomWindowFadeOut :"' .$settings['zwFade'].'"';
               $options[] = 'zoomWindowPosition:' .$zwPositioning;
               $options[] = 'scrollZoom      : ' .$settings['mousewheelZoom'];
               $options[] = 'easingAmount  : "'.$settings['zwEasing'].'"';
                if ( $settings['tint'] == true ) {
                    $options[] = 'tint     : true';
                    $options[] = 'tintColour:  "' . $settings['tintColor'] . '"';
                    $options[] = 'tintOpacity:  "' . $settings['tintOpacity'] . '"';
                }

                break;
        }
        if (count($options) == 0) return false;

        $options[] = 'customText:   "' . $settings['customText'] . '"';
        $options[] = 'customTextSize:   "' . $settings['customTextSize'] . '"';
        $options[] = 'customTextColor:   "' . $settings['customTextColor'] . '"';
        $valign = 'bottom';
        if ( strstr( $settings['customTextAlign'], 'top') ) {
            $valign = 'top';
        }
        $options[] = 'customTextVAlign:   "' . $valign . '"';
        $align = 'right';
        if ( strstr( $settings['customTextAlign'] , 'left' ) ) {
            $align = 'left';
        }
        if ( strstr( $settings['customTextAlign'] , 'center' ) ) {
            $align = 'center';
        }
        $options[] = 'customTextAlign:   "' . $align . '"';

        if ( $settings['onClick'] == 'true' ) {
            $options[] = 'onClick: true';
        }

        if ( $settings['ratio'] != 'default' ) {
            $options[] = 'ratio: ' . $settings['ratio'];
        
        }

        $options = implode(', ' . PHP_EOL . "\t\t", $options);

        return $options;
    }


    /**
     * Check the validity of the settings. The validity has to be the same as the javascript validation in image-zoom.settings.js
     * @access public
     */
    public function validate_settings( $post ) {
        $settings = $this->get_settings();

        /*
        $new_settings = array();
        foreach ( $settings as $_key => $_value ) {
            if ( isset( $post[$_key] ) && $post[$_key] != $_value['value'] ) {
                $new_settings[$_key] = $post[$_key]; 
            } else {
                $new_settings[$_key] = $_value['value'];
            } 
        }
         */
        $new_settings = $this->fill_defaults( $post );

        $new_settings['lensShape'] = $this->validateValuesSet('lensShape', $new_settings['lensShape']);
        $new_settings['cursorType'] = $this->validateValuesSet('cursorType', $new_settings['cursorType']);
        $new_settings['onClick'] = $this->validateValuesSet('onClick', $new_settings['onClick']);
        $new_settings['ratio'] = $this->validateValuesSet('ratio', $new_settings['ratio']);
        $new_settings['zwEasing'] = $this->validateRange('zwEasing', $new_settings['zwEasing'], 'int', 0, 200);
        $new_settings['lensSize'] = $this->validateRange('lensSize', $new_settings['lensSize'], 'int', 20, 2000);
        $new_settings['lensColour'] = $this->validateColor('lensColour', $new_settings['lensColour']);
        $new_settings['lensOverlay'] = $this->validateCheckbox('lensOverlay', $new_settings['lensOverlay']);
        $new_settings['borderThickness'] = $this->validateRange('borderThickness', $new_settings['borderThickness'], 'int', 0, 200);
        $new_settings['borderColor'] = $this->validateColor('borderColor', $new_settings['borderColor']);
        $new_settings['lensFade'] = $this->validateRange('lensFade', $new_settings['lensFade'], 'float', 0, 10);
        $new_settings['tint'] = $this->validateCheckbox('tint', $new_settings['tint']);
        $new_settings['tintColor'] = $this->validateColor('tintColor', $new_settings['tintColor']);
        $new_settings['tintOpacity'] = $this->validateRange('tintOpacity', $new_settings['tintOpacity'], 'float', 0, 1);
        $new_settings['zwWidth'] = $this->validateRange('zwWidth', $new_settings['zwWidth'], 'int', 0, 2000);
        $new_settings['zwHeight'] = $this->validateRange('zwHeight', $new_settings['zwHeight'], 'int', 0, 2000);
        $new_settings['zwResponsive'] = $this->validateCheckbox('zwResponsive', $new_settings['zwResponsive']);
        $new_settings['zwResponsiveThreshold'] = $this->validateRange('zwResponsiveThreshold', $new_settings['zwResponsiveThreshold'], 'int', 1, 3000 );
        $new_settings['zwPadding'] = $this->validateRange('zwPadding', $new_settings['zwPadding'], 'int', 0, 200 );
        $new_settings['zwPositioning'] = $this->validateValuesSet('zwPositioning', $new_settings['zwPositioning']);
        $new_settings['zwBorderThickness'] = $this->validateRange('zwBorderThickness', $new_settings['zwBorderThickness'], 'int', 0, 200);
        $new_settings['zwBorderRadius'] = $this->validateRange('zwBorderRadius', $new_settings['zwBorderRadius'], 'int', 0, 500);
        $new_settings['zwShadow'] = $this->validateRange('zwShadow', $new_settings['zwShadow'], 'int', 0, 500);
        $new_settings['zwFade'] = $this->validateRange('zwFade', $new_settings['zwFade'], 'float', 0, 10);
        $new_settings['mousewheelZoom'] = $this->validateCheckbox('mousewheelZoom', $new_settings['mousewheelZoom']);
        $new_settings['customText'] = $this->validateText('customText', $new_settings['customText'], 0, 1000);
        $new_settings['customTextSize'] = $this->validateRange('customTextSize', $new_settings['customTextSize'], 'int', 0, 45);
        $new_settings['customTextColor'] = $this->validateColor('customTextColor', $new_settings['customTextColor']);
        $new_settings['customTextAlign'] = $this->validateValuesSet('customTextAlign', $new_settings['customTextAlign']);

        return $new_settings; 
    }

    public function validate_general( $post = null) {
        $settings = $this->get_settings();

        if( $post == null ) {
            return array(
                'enable_woocommerce' => true,
                'exchange_thumbnails' => true,
                'enable_mobile' => false,
                'woo_variations' => false,
                'woo_cat' => false,
                'force_woocommerce' => true,
                'remove_lightbox_thumbnails' => false,
                'remove_lightbox' => false,
                'force_attachments' => false,
                'enable_fancybox' => false,
                'enable_photo_gallery' => false,
                'enable_jetpack_carousel' => false,
                'custom_class' => '',
                'flexslider' => '',
                'huge_it_gallery' => '',
            );
        }

        if ( ! isset( $post['enable_woocommerce'] ) ) 
            $post['enable_woocommerce'] = false;
        if ( ! isset( $post['exchange_thumbnails'] ) ) 
            $post['exchange_thumbnails'] = false;
        if ( ! isset( $post['enable_mobile'] ) ) 
            $post['enable_mobile'] = false;
        if ( ! isset( $post['woo_variations'] ) ) 
            $post['woo_variations'] = false;
        if ( ! isset( $post['woo_cat'] ) ) 
            $post['woo_cat'] = false;
        if ( ! isset( $post['force_woocommerce'] ) ) 
            $post['force_woocommerce'] = false;
        if ( ! isset( $post['remove_lightbox_thumbnails'] ) ) 
            $post['remove_lightbox_thumbnails'] = false;
        if ( ! isset( $post['remove_lightbox'] ) ) 
            $post['remove_lightbox'] = false;
        if ( ! isset( $post['force_attachments'] ) ) 
            $post['force_attachments'] = false;
        if ( ! isset( $post['enable_fancybox'] ) ) 
            $post['enable_fancybox'] = false;
        if ( ! isset( $post['enable_photo_gallery'] ) ) 
            $post['enable_photo_gallery'] = false;
        if ( ! isset( $post['enable_jetpack_carousel'] ) ) 
            $post['enable_jetpack_carousel'] = false;
        if ( ! isset( $post['custom_class'] ) ) 
            $post['custom_class'] = '';
        if ( ! isset( $post['flexslider'] ) ) 
            $post['flexslider'] = '';
        if ( ! isset( $post['huge_it_gallery'] ) ) 
            $post['huge_it_gallery'] = '';

        $new_settings = array(
            'enable_woocommerce' => $this->validateCheckbox('enable_woocommerce', $post['enable_woocommerce']),
            'exchange_thumbnails' => $this->validateCheckbox('exchange_thumbnails', $post['exchange_thumbnails']),
            'enable_mobile' => $this->validateCheckbox('enable_mobile', $post['enable_mobile']),
            'woo_variations' => $this->validateCheckbox('woo_variations', $post['woo_variations']),
            'woo_cat' => $this->validateCheckbox('woo_cat', $post['woo_cat']),
            'force_woocommerce' => $this->validateCheckbox('force_woocommerce', $post['force_woocommerce']),
            'remove_lightbox' => $this->validateCheckbox('remove_lightbox', $post['remove_lightbox']),
            'remove_lightbox_thumbnails' => $this->validateCheckbox('remove_lightbox_thumbnails', $post['remove_lightbox_thumbnails']),
            'force_attachments' => $this->validateCheckbox('force_attachments', $post['force_attachments']),
            'enable_fancybox' => $this->validateCheckbox('enable_fancybox', $post['enable_fancybox']),
            'enable_photo_gallery' => $this->validateCheckbox('enable_photo_gallery', $post['enable_photo_gallery']),
            'enable_jetpack_carousel' => $this->validateCheckbox('enable_jetpack_carousel', $post['enable_jetpack_carousel']),
            'custom_class' => $this->validateText('custom_class', $post['custom_class'], 0, 1000),
            'flexslider' => $this->validateText('flexslider', $post['flexslider'], 0, 1000),
            'huge_it_gallery' => $this->validateText('huge_it_gallery', $post['huge_it_gallery'], 0, 30),
        );

        return $new_settings;
    }

    public function fill_defaults( $settings = array() ) {
        $defaults = $this->get_settings();


        /* Rewrite the defaults for the pro version fields */
        if ( $settings['zwPositioning'] === '' ) {
            $pro_fields = array(
                'remove_lightbox_thumbnails', 'remove_lightbox', 'force_attachments', 'enable_fancybox', 'enable_photo_gallery', 'enable_jetpack_carousel', 'custom_class', 'flexslider', 'huge_it_gallery', 'onClick', 'ratio', 'lensColour', 'lensOverlay', 'zwResponsive', 'zwResponsiveThreshold', 'zwPositioning', 'mousewheelZoom', 'customText', 'customTextSize', 'customTextColor', 'customTextAlign',
            );

            foreach( $pro_fields as $_field ) {
                unset( $settings[$_field] );
            }
        }

        $new_settings = array();
        foreach ( $defaults as $_key => $_value ) {
            if( $_value['input_form'] == 'checkbox' ) {
                if ( !isset( $settings[$_key] ) || $settings[$_key] == false ) {
                    $new_settings[$_key] = false;
                } else {
                    $new_settings[$_key] = true;
                }
            } elseif ( isset( $settings[$_key] ) && $settings[$_key] != $_value['value'] ) {
                $new_settings[$_key] = $settings[$_key]; 
            } else {
                $new_settings[$_key] = $_value['value'];
            } 
        }

        return $new_settings;
    }

    /**
     * Helper to validate a checkbox
     * @access private
     */
    private function validateCheckbox( $id, $value ) {
        $settings = $this->get_settings();

        if ( $value == 'on' ) $value = true;

        if ( !is_bool($value) ) {
            $value = $settings[$id]['value'];
            $this->add_message('info', __('Unrecognized <b>'.$settings[$id]['label'].'</b>. The value was reset to default', 'zoooom') );
        } else {
        }
        return $value;
    }

    /**
     * Helper to validate a color
     * @access private
     */
    private function validateColor( $id, $value ) {
        $settings = $this->get_settings();

        if ( !preg_match('/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/', $value) ) {
            $value = $settings[$id]['value'];
            $this->add_message('info', __('Unrecognized <b>'.$settings[$id]['label'].'</b>. The value was reset to <b>'.$settings[$id]['value'] . '</b>', 'zoooom') );
        }
        return $value;
    }

    /**
     * Helper to validate a text
     * @access private
     */
    private function validateText( $id, $value, $min_size, $max_size ) {
        return filter_var($value, FILTER_SANITIZE_STRING);
    }

    /**
     * Helper to validate the value out of a set of values
     * @access private
     */
    private function validateValuesSet( $id, $value ) {
        $settings = $this->get_settings();

        if ( !array_key_exists($value, $settings[$id]['values']) ) {
            $value = $settings[$id]['value'];
            $this->add_message('info', __('Unrecognized <b>'.$settings[$id]['label'].'</b>. The value was reset to <b>'.$settings[$id]['value'] . '</b>', 'zoooom') );
        }
        return $value;
    }

    /**
     * Helper to validate an integer of a float
     * @access private
     */
    private function validateRange( $id, $value, $type, $min, $max ) {
        $settings = $this->get_settings();

        if ( $type == 'int' ) $new_value = (int)$value;
        if ( $type == 'float' ) $new_value = (float)$value;

        if ( !is_numeric($value) || $new_value < $min || $new_value > $max ) {
            $new_value = $settings[$id]['value'];
            $this->add_message('info', __('<b>'.$settings[$id]['label'].'</b> accepts values between '.$min.' and '.$max .'. Your value was reset to <b>' . $settings[$id]['value'] .'</b>', 'zoooom') );
        }
        return $new_value;
    }


    /**
     * Add a message to the $this->messages array
     * @type    accepted types: success, error, info, block
     * @access private
     */
    private function add_message( $type = 'success', $text ) {
        global $comment;
        $messages = $this->messages;
        $messages[] = array('type' => $type, 'text' => $text);
        $comment[] = array('type' => $type, 'text' => $text);
        $this->messages = $messages;
    }

    /**
     * Output the form messages
     * @access public
     */
    public function show_messages() {
        global $comment;
        if ( sizeof( $comment ) == 0 ) return;
        $output = '';
        foreach ( $comment as $message ) {
            $output .= '<div class="alert alert-'.$message['type'].'">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  '. $message['text'] .'</div>';
        }
        return $output;
    }



    /**
     * Add a button to the TinyMCE toolbar
     * @access public
     */
    function iz_add_tinymce_button() {
        global $typenow;

        if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
            return;
        }
        if ( in_array( $typenow, array( 'attachment', 'revision', 'nav_menu_item' ) ) )
            return; 

        if ( isset( $_GET['page'] ) && $_GET['page'] == 'wplister-templates' ) 
            return;

        if ( get_user_option('rich_editing') != 'true') 
            return;

        add_filter('mce_external_plugins', array( $this, 'iz_add_tinymce_plugin' ) );
        add_filter('mce_buttons', array( $this, 'iz_register_tinymce_button' ) );
    }

    /**
     * Register the plugin with the TinyMCE plugins manager
     * @access public
     */
    function iz_add_tinymce_plugin($plugin_array) {
        $plugin_array['image_zoom_button'] = $this->asset_url. 'js/tinyMCE-button.js?v=' . $this->main->version; 
        return $plugin_array;
    }

    /**
     * Register the button with the TinyMCE manager
     */
    function iz_register_tinymce_button($buttons) {
        array_push($buttons, 'image_zoom_button');
        return $buttons;
    }


    /**
     * Initiate the EDD_SL_Plugin_Updater class
     */
    public function edd_updater() {
        if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
            include( 'edd/EDD_SL_Plugin_Updater.php' );
        }

        $license_key = trim( get_option( 'image_zoom_license_key' ) ); 
        $beta = (trim( get_option( 'image_zoom_license_beta' ) ) == '1' ) ? true : false;

        $edd_updater = new EDD_SL_Plugin_Updater( 
            $this->main->plugin_server,
            $this->main->file, 
            array(
                'version'   => $this->main->version, 
                'license'   => $license_key, 
                'item_name' => $this->main->plugin_name,
                'author'    => $this->main->author,
                'url'       => home_url(),
                'beta'		=> $beta, 
            ) );
    }


}


return new ImageZoooom_Admin_PRO();
