<?php

require_once 'image-zoom-forms-helper.php';

$iz = ImageZoooomPRO();
$iz_admin = new ImageZoooom_Admin_PRO;
$iz_forms_helper = new ImageZoooom_FormsHelper;

$assets_url = $iz->plugins_url() . '/assets';

$settings = $iz->get_option_general();
if ( $settings == false ) {
    $settings = $iz_admin->validate_general( null );
}

$messages = $iz_admin->show_messages();

?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>


<h2>WP Image Zoom PRO by <img src="<?php echo site_url(); ?>/wp-content/plugins/wp-image-zoooom-pro/assets/images/silkypress_logo.png" /> <a href="https://www.silkypress.com/" target="_blank">SilkyPress.com</a></h2>

<div class="wrap">

<h3 class="nav-tab-wrapper woo-nav-tab-wrapper">
    <a href="?page=zoooom_settings&tab=general" class="nav-tab nav-tab-active"><?php echo __('General Settings', 'zoooom'); ?></a>
    <a href="?page=zoooom_settings&tab=settings" class="nav-tab"><?php echo __('Zoom Settings'); ?></a>
    <a href="?page=zoooom_settings&tab=license" class="nav-tab"><?php echo __('License Key'); ?></a>
</h3>

<div class="panel panel-default">
    <div class="panel-body">
    <div class="row">



    <div class="col-lg-12">
    <?php echo $messages ?>
    <div id="alert_messages">
    </div>
    </div>


        

<form class="form-horizontal" method="post" action="" id="form_settings">

        <?php
        $iz_forms_helper->label_class = 'col-sm-6 control-label';

        foreach ( array('enable_woocommerce', 'exchange_thumbnails', 'woo_cat', 'woo_variations', 'enable_mobile', 'remove_lightbox_thumbnails', 'remove_lightbox', 'force_attachments', 'custom_class', 'flexslider', 'huge_it_gallery', 'enable_photo_gallery', 'enable_fancybox', 'enable_jetpack_carousel' ) as $_field ) {
            $this_settings = $iz_admin->get_settings( $_field);
            $this_settings['value'] = $settings[$_field];
            $iz_forms_helper->input($this_settings['input_form'], $this_settings); 
        }
        
        ?> 

<div class="form-group">
      <div class="col-lg-6">
        <input type="hidden" name="tab" value="general" />
          <button type="submit" class="btn btn-primary"><?php echo __('Save changes', 'zoooom'); ?></button>
      </div>
    </div>

    <?php wp_nonce_field( 'iz_general' ); ?>

</form>


    </div>
    </div>
</div>
</div>

<?php include_once('right_columns.php'); ?>
