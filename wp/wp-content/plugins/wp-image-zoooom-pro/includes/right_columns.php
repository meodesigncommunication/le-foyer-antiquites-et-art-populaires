<div id="right_column_metaboxes">
 
    <div class="panel main_container">
    <div class="container_title">
        <h3><?php _e('Like this Plugin?'); ?></h3>
    </div>
        <div class="metabox-holder rating" style="text-align: center;"> 
        <p><?php echo __('Share your opinion with the world on the WordPress.org Plugin Repository.'); ?></p>
        <p><a href="https://wordpress.org/plugins/wp-image-zoooom/" target="_blank" class="button"><?php _e('Rate it on WordPress.org'); ?></a></p>
        </div> 
    </div>   
</div>

<div style="clear: both"></div>

