<?php
/**
 * Plugin Name: WP Image Zoom PRO
 * Plugin URI: https://www.silkypress.com
 * Description: Add zoom effect over the an image, whether it is an image in a post/page or the featured image of a product in a WooCommerce shop 
 * Version: 1.21
 * Author: SilkyPress
 * Author URI: https://www.silkypress.com
 *
 * Text Domain: zoooom
 * Domain Path: /languages/
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'ImageZoooomPRO' ) ) :
/**
 * Main ImageZoooom Class
 *
 * @class ImageZoooom
 */
final class ImageZoooomPRO {
    public $version = '1.21';
    public $testing = false;
    public $free = false;
    public $options_general = array();
    public $file = __FILE__;
    public $plugin_name = 'WP Image Zoooom';
    public $plugin_server = 'https://www.silkypress.com';
    public $author = 'Diana Burduja';

    protected static $_instance = null; 


    /**
     * Main ImageZoooom Instance
     *
     * Ensures only one instance of ImageZoooom is loaded or can be loaded
     *
     * @static
     * @return ImageZoooom - Main instance
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
      * Cloning is forbidden.
      */
    public function __clone() {
         _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'zoooom' ), '1.0' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'zoooom' ), '1.0' );
    }

    /**
     * Image Zoooom Constructor
     * @access public
     * @return ImageZoooom
     */
    public function __construct() {
         if ( is_admin() ) {
            include_once( 'includes/image-zoom-admin.php' );
            include_once( 'includes/image-zoom-warnings.php' );
         }
        $this->options_general = $this->get_option_general();
        add_action( 'template_redirect', array( $this, 'template_redirect' ) );
        add_action( 'vc_after_init', array( $this, 'js_composer' ) );

        if ( strpos( get_template(), 'enfold' ) !== false ) {
            add_theme_support('avia_template_builder_custom_css');
        }

    }

    function js_composer() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) return false;
        $param = WPBMap::getParam( 'vc_single_image', 'style' );
        if ( !isset($param) || !isset($param['value']) ) return; 
        $param['value'][__( 'WP Image Zoooom', 'zoooom' )] = 'zoooom';
        vc_update_shortcode_param( 'vc_single_image', $param );
    }

    /**
     * Show the javascripts in the front-end
     * Hooked to template_redirect in $this->__construct()
     * @access public
     */
    public function template_redirect() {

        $opt = $this->options_general;

        if ( isset($opt['enable_mobile']) && empty($opt['enable_mobile']) && wp_is_mobile() )
            return false;

        // Adjust the zoom to WooCommerce 3.0.+
        if ( $opt['enable_woocommerce'] && class_exists('woocommerce') && version_compare( WC_VERSION, '3.0', '>') ) {
            remove_theme_support( 'wc-product-gallery-zoom' );
            if ( $opt['remove_lightbox'] ) {
                remove_theme_support( 'wc-product-gallery-lightbox' );
            } else {
                add_theme_support( 'wc-product-gallery-lightbox' );
            }
            add_theme_support( 'wc-product-gallery-slider' );
        }

        add_filter( 'woocommerce_single_product_image_html', array( $this, 'woocommerce_single_product_image_html' ) );
        add_filter( 'woocommerce_single_product_image_thumbnail_html', array( $this, 'woocommerce_single_product_image_thumbnail_html' ) );

//        if ( isset($opt['remove_lightbox']) && $opt['remove_lightbox'] == 1 ) {
            add_filter( 'woocommerce_single_product_image_html', array( $this, 'remove_prettyPhoto' ) );
            add_filter( 'woocommerce_single_product_image_thumbnail_html', array( $this, 'remove_prettyPhoto' ) );
 //       }
        add_filter( 'the_content', array( $this, 'dt_7_compatibility' ), 40 );
        add_filter( 'the_content', array( $this, 'find_bigger_image' ), 40 );
        add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );
        add_action( 'wp_head', array( $this, 'wp_head_compatibilities' ) );
        add_action( 'the_content', array( $this, 'the_content_compatibilities' ), 40 );

        add_filter( 'wp_calculate_image_srcset', array( $this, 'wp_calculate_image_srcset' ), 40, 5  );
    }


    /**
     * If the full image isn't in the srcset, then add it
     */
    function wp_calculate_image_srcset($sources, $size_array, $image_src, $image_meta, $attachment_id ) {
        if ( ! isset( $image_meta['width'] ) ) {
            return $sources;
        }
        if ( ! is_array( $sources ) ) {
            $sources = array();
        }
        if ( isset( $sources[ $image_meta['width'] ] ) ) {
            return $sources;
        }

        if ( is_array($size_array) && count($size_array) == 2 && isset($image_meta['height']) && isset($image_meta['width'])) {
            $ratio = $size_array[0] * $image_meta['height'] / $size_array[1] / $image_meta['width'];
            if ( $ratio > 1.03 || $ratio < 0.97 ) return $sources;
        }

        $url = str_replace( wp_basename( $image_src ), wp_basename( $image_meta['file'] ), $image_src );
        $sources[$image_meta['width']] = array(
                'url' => $url, 
                'descriptor' => 'w',
                'value' => $image_meta['width'], 
        );
        return $sources;
    } 


    /**
     * Add data-thumbnail-src to the main product image 
     */
    function woocommerce_single_product_image_html( $content ) {
        if ( !strstr( $content, 'attachment-shop_single' ) ) {
            $content = preg_replace('/ class="([^"]+)" alt="/i', ' class="attachment-shop_single $1" alt="', $content);
        }
        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'shop_thumbnail' );

        if ( ! isset( $thumbnail[0] ) ) return $content;

        $thumbnail_data = ' data-thumbnail-src="'.$thumbnail[0].'"';

        return str_replace( ' title="', $thumbnail_data . ' title="', $content );
    }

    /**
     * Force the WooCommerce to use the "src" attribute
     */
    function woocommerce_single_product_image_thumbnail_html( $content ) {
        $content = str_replace('class="attachment-shop_single size-shop_single"', 'class="attachment-shop_thumbnail size-shop_thumbnail"', $content);
        
        if ( !strstr( $content, 'attachment-shop_thumbnail' ) ) {
            $content = str_replace(' class="', ' class="attachment-shop_thumbnail ', $content);
        }

        if ( strstr( $content, 'attachment-shop_single' ) ) {
            $content = str_replace('attachment-shop_single', '', $content);
        }
        return $content;
    }

    /**
     * Remove the lightbox
     */
    function remove_prettyPhoto( $content ) {
        $replace = array( 'data-rel="prettyPhoto"', 'data-rel="lightbox"', 'data-rel="prettyPhoto[product-gallery]"', 'data-rel="lightbox[product-gallery]"', 'data-rel="prettyPhoto[]"', ' rel="prettyPhoto[product-gallery]"'); 

        return str_replace( $replace, 'data-rel="zoomImage[product-gallery]"', $content );
    }


    /**
     * Dt 7 gallery theme compatibility
     */
    function dt_7_compatibility( $content ) {
        if ( ! strstr( $content, 'dt-gallery-container' ) ) 
            return $content;

        preg_match_all( '/srcset="([^ ]+) ([0-9])x, ([^ ]+) ([0-9])x"/', $content, $matches );

        if ( isset( $matches[0] ) && count( $matches ) == 0 )
            return $content;

        foreach( $matches[0] as $_key => $_image ) {

            $img_src = preg_replace( '@(-[0-9]+x[0-9]+).(jpg|png|gif)@', '.$2', $matches[1][$_key] );
            $content = str_replace( $_image, $_image . ' src="'.$img_src.'"', $content );
        }

        $content = str_replace('rollover rollover-zoom', 'zoomGallery', $content );

        return $content;
    }

    /**
     * Find bigger image if class="zoooom" and there is no srcset
     *
     * Note: the srcset is not be set if for some reason 
     *      the _wp_attachment_metadata for the image is not present
     */
    function find_bigger_image( $content ) {
        if ( ! preg_match_all( '/<img [^>]+>/', $content, $matches ) ) {
            return $content;
        }

        foreach( $matches[0] as $image ) {
            // the image has to have the class "zoooom"
            if ( false === strpos( $image, 'zoooom' ) ) {
                continue;
            }
            // the image was tagged to skip this step
            if ( false !== strpos( $image, 'skip-data-zoom-image' ) ) {
                continue;
            }
            // the image does not have the srcset
            if ( false !== strpos( $image, ' srcset=' ) ) {
                continue;
            }
            // the image does not have an "-300x400.jpg" type ending
            if ( 0 == preg_match( '@ src="([^"]+)(-[0-9]+x[0-9]+).(jpg|png|gif)"@', $image) ) {
                continue;
            }

            // link the full-sized image to the data-zoom-image attribute
            $full_image = preg_replace( '@^(.*) src="(.*)(-[0-9]+x[0-9]+).(jpg|png|gif)"(.*)$@',   '$2.$4', $image );
            $full_image_attr = ' data-zoom-image="' . $full_image . '"'; 
            $full_image_img = str_replace(' src=', $full_image_attr. ' src=', $image);
            $content = str_replace( $image, $full_image_img, $content);
        }

        return $content;
    }


    /**
     * WP_head compatibilities
     */
    function wp_head_compatibilities() {
        $theme = get_template();

        // The Bridge, Nouveau and Stockholm themes add a wrapper on the whole page with index higher than the zoom
        if ( strpos( $theme, 'bridge' ) !== false || strpos( $theme, 'nouveau' ) !== false || strpos( $theme, 'stockholm' ) !== false ) { ?>
            <style type="text/css"> .wrapper { z-index: 40 !important; } </style>
        <?php }


        // For the Divi the user agent styles are interfering with the FlexSlider
        if ( strpos($theme, 'Divi') !== false ) { ?>
            <style type="text/css">figure {display: block;-webkit-margin-before: 0;-webkit-margin-after: 0;-webkit-margin-start: 0;-webkit-margin-end: 0;}</style>
        <?php }


        // The Enfold theme adds an image-overlay that hides the zoom
        if ( strpos( $theme, 'enfold' ) !== false ) {
            echo '<style type="text/css">.product .image-overlay, .zoooom .image-overlay { display: none !important; } </style>';
            echo '<script type="text/javascript">
                jQuery(document).ready(function( $ ){
                    var cw = $(".thumbnails img").width();
                    $(".thumbnails img").css({"height":cw+"px"});
                });
            </script>';

            // for the WooCommerce 3.0.+ version 
            if (class_exists('woocommerce') && version_compare( WC_VERSION, '3.0', '>') ) {
                ?>
                <script type="text/javascript">
                jQuery(window).ready(function() {
                    jQuery('.woocommerce-product-gallery__wrapper img').first().removeClass('attachment-shop_thumbnail').addClass('attachment-shop_single');    
                    jQuery('.attachment-shop_single').image_zoom( IZ.options);
                });
                </script>
                <?php
            }
        }

        // Remove the image overlay for the HyperX theme
        if ( strpos($theme, 'hyperx') !== false ) { ?>
            <style type="text/css">.image-overlay { display: none !important;}</style>
        <?php }

 
        // Remove the hover effect on the LoveStory theme 
        if ( strpos( $theme, 'lovestory' ) !== false ) {
            echo '<style type="text/css"> .hover_icon:after {content:normal !important;}.hover_icon_link:before {content: normal !important;} .hover_icon_view:before { content:  normal; }</style>' . PHP_EOL;
        }


        // When there is a tint, the prettyPhoto gallery breaks
        $settings = get_option( 'zoooom_settings_js' );
        if ( strpos( $settings, 'tintColour' ) !== false ) : ?>
            <style type="text/css">div.pp_woocommerce .pp_content_container{background:transparent !important;}</style>
        <?php
        endif;


         // If the onClick option is set, change "cursor: zoom-in" on image hover
        if ( strpos( $settings, 'onClick' ) !== false ) : ?>
          <style type="text/css">img.zoooom:hover, .zoooom img:hover, .mp_product_image_single:hover, a.zoomGallery img:hover, .portfolio_images img:hover, .single-fluxus_portfolio .project-image img:hover, .attachment-product_page_image:hover, .attachment-shop_single:hover {cursor:-webkit-zoom-in;cursor:-moz-zoom-in;cursor:zoom-in;}</style>
        <?php endif;


    }


    /**
     * the_content compatibilities
     */
    function the_content_compatibilities( $content ) {
        $theme = get_template();

        // Theme Divi compatibility with Gallery module
        if ( strpos( $theme, 'divi' ) !== false && strpos( $content, 'et_pb_gallery_grid' ) !== false ) {
            $content = preg_replace('@<span class="et_overlay"></span>@i', '', $content);
        }


        // Theme Avia compatibility with the gallery
        if ( strpos( $theme, 'avia' ) !== false && strpos( $content, 'avia-gallery' ) !== false ) {
            $content = str_replace('fakeLightbox', 'noHover noLightbox zoomGallery', $content );

            $content = preg_replace( '@(-[0-9]+x[0-9]+).(jpg|png|gif)@', '.$2', $content );
        }

        return $content;
    }


    /**
     * Enqueue the jquery.image_zoom.js
     * Hooked to wp_enqueue_scripts in $this->template_redirect
     * @access public
     */
    public function wp_enqueue_scripts() {

        $opt = $this->options_general;

        $prefix = '.min';
        if ( $this->testing == true ) {
            $prefix = '';
        }
        $in_footer = false;

        if ( strpos( strtolower(get_template()), 'avada' ) !== false ) {
            $in_footer = true;
        }

        // Load the jquery.image_zoom.js
        wp_register_script( 'image_zoooom', $this->plugins_url( '/assets/js/jquery.image_zoom' . $prefix . '.js' ), array( 'jquery' ), $this->version, $in_footer );
        wp_enqueue_script( 'image_zoooom' );

        // Load the image_zoom-init.js
        wp_register_script( 'image_zoooom-init', $this->plugins_url( '/assets/js/image_zoom-init.js' ), array( 'jquery'), $this->version, $in_footer );
        wp_localize_script( 'image_zoooom-init', 'IZ', $this->get_localize_vars());
        wp_enqueue_script( 'image_zoooom-init' );

        // Remove the prettyPhoto 
        if ( isset( $opt['remove_lightbox'] ) && $opt['remove_lightbox'] == 1 ) {
            wp_dequeue_script( 'prettyPhoto' );
            wp_dequeue_script( 'prettyPhoto-init' );
        }

        // Remove the prettyPhoto on thumbnails
        if ( isset( $opt['remove_lightbox_thumbnails'] ) && $opt['remove_lightbox_thumbnails'] == 1 ) {
            wp_dequeue_script( 'prettyPhoto-init' );
        }
    }



    function get_localize_vars() {
        $general = $this->get_option_general();
        $options = $this->get_options_for_zoom();

        $default = array(
            'options' => $options,
            'with_woocommerce' => '1',
            'exchange_thumbnails' => '1',
            'woo_categories' => (isset($general['woo_cat']) && $general['woo_cat'] == 1 ) ? '1' : '0',
            'force_attachments' => (isset($general['force_attachments']) && $general['force_attachments'] == 1 ) ? '1' : '0',
            'enable_fancybox' => (isset($general['enable_fancybox']) && $general['enable_fancybox'] == 1 ) ? '1' : '0',
            'enable_photo_gallery' => (isset($general['enable_photo_gallery']) && $general['enable_photo_gallery'] == 1 ) ? '1' : '0',
            'enable_jetpack_carousel' => (isset($general['enable_jetpack_carousel']) && $general['enable_jetpack_carousel'] == 1 ) ? '1' : '0',
            'custom_class' => $general['custom_class'],
            'flexslider' => '',
            'huge_it_gallery' => $general['huge_it_gallery'],
            'search_filter_pro' => ( defined( 'SEARCH_FILTER_PRO_BASE_PATH' ) ) ? '1' : '0', 
            'remove_lightbox_thumbnails' => (isset($general['remove_lightbox_thumbnails']) && $general['remove_lightbox_thumbnails'] == 1 ) ? '1' : '0',
            'remove_lightbox' => (isset($general['remove_lightbox']) && $general['remove_lightbox'] == 1 ) ? '1' : '0',
            'woo_slider' => '0', 
        );

        if (class_exists('woocommerce') && version_compare( WC_VERSION, '3.0', '>') && current_theme_supports( 'wc-product-gallery-slider' )) {
            $default['woo_slider'] = 1;
        }

        $with_woocommerce = true;
        if ( ! $this->woocommerce_is_active() )
            $default['with_woocommerce'] = '0';

        if ( isset($general['enable_woocommerce']) && empty($general['enable_woocommerce']))
            $default['with_woocommerce'] = '0';

        if ( isset($general['exchange_thumbnails']) && empty($general['exchange_thumbnails']))
            $default['exchange_thumbnails'] = '0';

        if ( isset( $general['flexslider'] ) && ! empty( $general['flexslider'] ) ) {
            $default['flexslider'] = trim($general['flexslider']);
            if ( strpos( $default['flexslider'], '.' ) === false ) {
                $default['flexslider'] = '.' . $default['flexslider'];
            }
        } 

        if ( false == $default['huge_it_gallery'] ) {
            $default['huge_it_gallery'] = '';   
        }

        if ( $default['with_woocommerce'] == '1' &&  strpos( get_template(), 'shopkeeper' ) !== false ) {
            $default['shopkeeper'] = '1'; 
        } 

        return $default;
    }




    function get_options_for_zoom() {
        $i = get_option( 'zoooom_settings' );
        $o = array();

        switch ( $i['lensShape'] ) {
            case 'none' : 
                $o = array(
                    'zoomType' => 'inner',
                    'cursor' => $i['cursorType'],
                    'easingAmount' => $i['zwEasing'],
                );
                break;
            case 'square' :
            case 'round' :
                $o = array(
                    'lensShape' => $i['lensShape'],
                    'zoomType' => 'lens',
                    'lensSize' => $i['lensSize'],
                    'borderSize' => $i['borderThickness'], 
                    'borderColour' => $i['borderColor'],
                    'cursor' => $i['cursorType'],
                    'lensFadeIn' => $i['lensFade'] * 1000,
                    'lensFadeOut' => $i['lensFade'] * 1000,
                );
                if ( $i['tint'] == true ) {
                    $o['tint'] = 'true';
                    $o['tintColour'] = $i['tintColor'];
                    $o['tintOpacity'] = $i['tintOpacity'];
                }
 
                break;
            case 'square' :
                break;
            case 'zoom_window' :
                switch( $i['zwPositioning'] ) {
                    case 'right_bottom' : $pos = 3; break;
                    case 'right_center' : $pos = 2; break;
                    case 'left_top' : $pos = 11; break;
                    case 'left_bottom' : $pos = 9; break;
                    case 'left_center' : $pos = 10; break;
                    default: $pos = 1;
                }
                if ( $i['lensOverlay'] == true ) 
                    $i['lensOverlay'] = $this->plugins_url() . '/assets/images/lens-overlay-1.png';
                $i['zwResponsive'] = ( $i['zwResponsive'] ) ? true : false;
                $i['lensOverlay'] = ( $i['lensOverlay'] ) ? true : false;
                $i['mousewheelZoom'] = ( $i['mousewheelZoom'] ) ? true : false;

                $o = array(
                   'lensShape' => 'square',
                   'lensSize' => $i['lensSize'], 
                   'lensBorderSize' => $i['borderThickness'], 
                   'lensBorderColour' => $i['borderColor'], 
                   'borderRadius' => $i['zwBorderRadius'], 
                   'cursor' => $i['cursorType'],
                   'zoomWindowWidth' => $i['zwWidth'],
                   'zoomWindowHeight' => $i['zwHeight'],
                   'zoomWindowOffsetx' => $i['zwPadding'],
                   'borderSize' => $i['zwBorderThickness'],
                   'borderColour' => $i['zwBorderColor'],
                   'zoomWindowShadow' => $i['zwShadow'],
                   'lensFadeIn' => $i['lensFade'] * 1000,
                   'lensFadeOut' => $i['lensFade'] * 1000,
                   'zoomWindowFadeIn' => $i['zwFade'] * 1000,
                   'zoomWindowFadeOut' => $i['zwFade'] * 1000,
                   'easingAmount  ' => $i['zwEasing'],
                   'zoomWindowPosition' => $pos,
                   'lensOverlay' => $i['lensOverlay'],
                   'responsive' => $i['zwResponsive'],
                   'responsiveThreshold' => $i['zwResponsiveThreshold'],
                   'scrollZoom' => $i['mousewheelZoom'],
                );

                if ( $i['tint'] == true ) {
                    $o['tint'] = 'true';
                    $o['tintColour'] = $i['tintColor'];
                    $o['tintOpacity'] = $i['tintOpacity'];
                }

                break;
        }

        $o['customText'] = $i['customText'];
        $o['customTextSize'] = $i['customTextSize'];
        $o['customTextColor'] = $i['customTextColor'];
        $o['customTextVAlign'] = 'bottom';
        if ( strstr( $i['customTextAlign'], 'top') ) {
            $o['customTextVAlign'] = 'top';
        }
        $o['customTextAlign'] = 'right';
        if ( strstr( $i['customTextAlign'] , 'left' ) ) {
            $o['customTextAlign'] = 'left';
        }
        if ( strstr( $i['customTextAlign'] , 'center' ) ) {
            $o['customTextAlign'] = 'center';
        }

        if ( $i['onClick'] == 'true' ) {
            $o['onClick'] = true;
        }

        if ( $i['ratio'] != 'default' ) {
            $o['ratio'] = $i['ratio'];
        }


        return $o;
    }


    /** Helper function ****************************************/

    public function plugins_url( $path  = '/' ) {
        return untrailingslashit( plugins_url( $path, __FILE__ ) );
    }

    public function plugin_dir_path() {
        return untrailingslashit( plugin_dir_path( __FILE__ ) );
    }

    /**
     * Check if WooCommerce is activated
     * @access public
     * @return bool
     */
    public function woocommerce_is_active() {
        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
            return true;
        }
        return false;
    }

    public function get_option_general() {
        $general = get_option('zoooom_general');

        if (!isset($general['enable_woocommerce']))
            $general['enable_woocommerce'] = true;

        if (!isset($general['exchange_thumbnails']))
            $general['exchange_thumbnails'] = true;

        if ( !isset( $general['enable_mobile'] ) )
            $general['enable_mobile'] = false;

        $general['force_woocommerce'] = false;

        if ( !isset( $general['woo_cat'] ) )
            $general['woo_cat'] = false;

        if ( ! $this->woocommerce_is_active() ) {
            $general['woo_cat'] = false;
        } 

        if ( !isset( $general['remove_lightbox_thumbnails'] ) )
            $general['remove_lightbox_thumbnails'] = false;

        if ( !isset( $general['remove_lightbox'] ) )
            $general['remove_lightbox'] = true;

        if ( !isset( $general['force_attachments'] ) )
            $general['force_attachments'] = false;

        if ( !isset( $general['custom_class'] ) )
            $general['custom_class'] = '';

        if ( !isset( $general['enable_fancybox'] ) )
            $general['enable_fancybox'] = false;

        if ( !isset( $general['enable_photo_gallery'] ) )
            $general['enable_photo_gallery'] = false;

        if ( !isset( $general['enable_jetpack_carousel'] ) )
            $general['enable_jetpack_carousel'] = false;

        if ( !isset( $general['flexslider'] ) )
            $general['flexslider'] = false;

        if ( !isset( $general['huge_it_gallery'] ) )
            $general['huge_it_gallery'] = false;

       return $general; 
    }

}

endif; 

/**
 * Returns the main instance of ImageZoooom
 *
 * @return ImageZoooom
 */
function ImageZoooomPRO() {
    return ImageZoooomPRO::instance();
}

ImageZoooomPRO();

function iz_plugin_settings_link($links) {

    $settings_link = '<a href="admin.php?page=zoooom_settings">'.__('Settings', 'zoooom').'</a>';
    array_push( $links, $settings_link );
    return $links;
}
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'iz_plugin_settings_link');



/**
 * Huge IT Gallery compatibility
 */
if ( ! function_exists( 'get_huge_image' ) ) {
    function get_huge_image( $image_url, $img_prefix ) {
        return $image_url;
    }
}

/**
 * Enfold theme compatibility
 */
if ( strpos( get_template(), 'enfold' ) !== false 
    && !function_exists('avia_woocommerce_gallery_thumbnail_description') 
    && class_exists('woocommerce') 
    && version_compare( WC_VERSION, '3.0', '>') ) {
add_filter('woocommerce_single_product_image_thumbnail_html','avia_woocommerce_gallery_thumbnail_description', 10, 4);
function avia_woocommerce_gallery_thumbnail_description($img, $attachment_id, $post_id = "", $image_class = "" ) {

    $image_link = wp_get_attachment_url( $attachment_id );

    if(!$image_link) return $img;

    $image = wp_get_attachment_image( $attachment_id, 'shop_single' );
    $image_title = esc_attr(get_post_field('post_content', $attachment_id));

    $img = sprintf( '<a href="%s" class="%s noLightbox" title="%s"  rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_title, $image );


    return $img;
}
}
