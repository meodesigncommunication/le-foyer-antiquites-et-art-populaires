jQuery(document).ready(function($) {


    function updateZoomer() {


        $("#demo").unbind( 'touchstart touchmove touchend click mouseenter' );

        var zoom_options = {};

        // Get the settings
        var settings = {
            lensShape       : $("input[name=lensShape]:checked").val(),
            cursorType      : $("input[name=cursorType]:checked").val(),
            onClick         : $("input[name=onClick]:checked").val(),
            zwEasing        : parseInt($("#zwEasing").val()),
            ratio           : $("input[name=ratio]:checked").val(),
            lensSize        : parseInt($("#lensSize").val()),
            lensColour      : $("#lensColour").val(),
            lensOverlay     : $("#lensOverlay").is(':checked'),
            borderThickness : parseInt($("#borderThickness").val()),
            borderColor     : $("#borderColor").val(),
            borderRadius    : parseInt($("#borderRadius").val()),
            zwWidth         : parseInt($("#zwWidth").val()),
            zwHeight        : parseInt($("#zwHeight").val()),
            zwResponsive    : $("#zwResponsive").is(':checked'),
            zwResponsiveThreshold : parseInt($("#zwResponsiveThreshold").val()),
            zwPositioning   : $("input[name=zwPositioning]:checked").val(),
            zwShadow        : parseInt($("#zwShadow").val()),
            zwPadding        : parseInt($("#zwPadding").val()),
            zwBorderThickness : parseInt($("#zwBorderThickness").val()),
            zwBorderColor   : $("#zwBorderColor").val(),
            zwBorderRadius  : parseInt($("#zwBorderRadius").val()),
            lensFade        : parseFloat($("#lensFade").val()) * 1000,
            zwFade          : parseFloat($("#zwFade").val()) * 1000,
            mousewheelZoom  : $("#mousewheelZoom").is(':checked'),
            tint            : $("#tint").is(':checked'),
            tintColor       : $("#tintColor").val(),
            tintOpacity     : parseFloat($("#tintOpacity").val()),
            customText      : $("#customText").val(),
            customTextSize  : parseInt($("#customTextSize").val()),
            customTextColor : $("#customTextColor").val(),
            customTextAlign : $("input[name=customTextAlign]:checked").val(),
        };

        validateInput( settings );

        if ( settings.tintOpacity > 1 ) {
            settings.tintOpacity = 1;
        } 
        if ( settings.tintOpacity < 0 ) {
            settings.tintOpacity = 0;
        }

        if ( settings.cursorType === 'zoom' ) {
            settings.cursorType = 'url(../images/cursor_type_zoom.svg) auto';
        }

        if ( settings.lensOverlay === true ) {
            settings.lensOverlay = '../images/lens-overlay-1.png';
//            settings.lensOverlay = '../images/lens-overlay-2.gif';
        }

        // Configure the zoomer
        switch (settings.lensShape) {
            case 'none' :
                zoom_options = {
                    zoomType    : "inner",
                    cursor      : settings.cursorType,
                    easingAmount    : settings.zwEasing,
                };
                break;
            case 'square' :
            case 'round' :
                zoom_options = {
                    lensShape       : settings.lensShape,
                    zoomType        : 'lens',
                    lensSize        : settings.lensSize, 
                    borderSize      : settings.borderThickness, 
                    borderColour    : settings.borderColor, 
                    cursor          : settings.cursorType,
                    lensFadeIn      : settings.lensFade,
                    lensFadeOut     : settings.lensFade,
                };
                if ( settings.tint === true) {
                    zoom_options.tint = true;
                    zoom_options.tintColour = settings.tintColor;
                    zoom_options.tintOpacity = settings.tintOpacity;
                }
                break;
            case 'zoom_window' :
                zoom_options = {
                    lensShape       : 'square',
                    lensSize        : settings.lensSize, 
                    lensBorderSize  : settings.borderThickness, 
                    lensBorderColour: settings.borderColor, 
                    lensColour      : settings.lensColour,
                    lensOverlay     : settings.lensOverlay,
                    borderRadius    : settings.zwBorderRadius, 
                    cursor          : settings.cursorType,
                    zoomWindowWidth : settings.zwWidth,
                    zoomWindowHeight: settings.zwHeight,
                    responsive      : settings.zwResponsive,
                    responsiveThreshold      : settings.zwResponsiveThreshold,
                    zoomWindowShadow: settings.zwShadow,
                    borderSize      : settings.zwBorderThickness,
                    borderColour    : settings.zwBorderColor,
                    zoomWindowOffsetx: settings.zwPadding,
                    lensFadeIn      : settings.lensFade,
                    lensFadeOut     : settings.lensFade,
                    zoomWindowFadeIn      : settings.zwFade,
                    zoomWindowFadeOut     : settings.zwFade,
                    scrollZoom      : settings.mousewheelZoom,
                    easingAmount    : settings.zwEasing,
                    zoomWindowPosition : 1,
                };
                if ( settings.tint === true) {
                    zoom_options.tint = true;
                    zoom_options.tintColour = settings.tintColor;
                    zoom_options.tintOpacity = settings.tintOpacity;
                }
                var demo_wrapper_align = 'left';
                if ( settings.zwPositioning.indexOf("right_bottom") >= 0 ) {
                    zoom_options.zoomWindowPosition = 3;
                }
                if ( settings.zwPositioning.indexOf("right_center") >= 0 ) {
                    zoom_options.zoomWindowPosition = 2;
                }

                if ( settings.zwPositioning.indexOf("left_top") >= 0 ) {
                    demo_wrapper_align = 'right';
                    $("#demo_wrapper").css('text-align', 'right');
                    zoom_options.zoomWindowPosition = 11;
                }
                if ( settings.zwPositioning.indexOf("left_bottom") >= 0 ) {
                    demo_wrapper_align = 'right';
                    zoom_options.zoomWindowPosition = 9;
                }
                if ( settings.zwPositioning.indexOf("left_center") >= 0 ) {
                    demo_wrapper_align = 'right';
                    zoom_options.zoomWindowPosition = 10;
                }
                $("#demo_wrapper").css('text-align', demo_wrapper_align);

                break;

        }
        if ( settings.onClick === 'true' ) {
            zoom_options.onClick = true; 
        }
        zoom_options.customText = settings.customText;
        zoom_options.customTextSize = settings.customTextSize;
        zoom_options.customTextColor = settings.customTextColor;
        zoom_options.customTextVAlign = 'bottom';
        if ( settings.customTextAlign.indexOf("top") >= 0 ) {
            zoom_options.customTextVAlign = 'top';
        }
        zoom_options.customTextAlign = 'right';
        if ( settings.customTextAlign.indexOf("left") >= 0 ) {
            zoom_options.customTextAlign = 'left';
        }
        if ( settings.customTextAlign.indexOf("center") >= 0 ) {
            zoom_options.customTextAlign = 'center';
        }

        if ( settings.ratio !== 'default' ) {
            zoom_options.ratio = settings.ratio; 
        }



        $("#demo").image_zoom(zoom_options);

        $(window).bind('resize', function() {
            $(window).resize(function() {
                clearTimeout(window.resizeEvt);
                window.resizeEvt = setTimeout(function() {
                    $(".zoomContainer").remove();
                    $("#demo").image_zoom(zoom_options);
                }, 300);
            });
        });

        enableDisableFields(settings);

    }

    $('[data-toggle="tooltip"]').tooltip();

    if ( $("#demo").length > 0 ) {
        updateZoomer();
    }

    function validateInput( settings ) {

        if ( isNaN(settings.zwEasing) || settings.zwEasing < 0 || settings.zwEasing > 200 ) {
            show_alert('<b>Animation Easing Effect</b> accepts integers between 0 and 200. Your value was stripped to 12');
            settings.zwEasing = 12;
            $("#zwEasing").val('12');
        }

        if ( isNaN(settings.lensSize) || settings.lensSize < 20 || settings.lensSize > 2000 ) {
           show_alert('<b>Lens Size</b> accepts integers between 20 and 2000. Your value was reset to 200');
           settings.lensSize = 200;
           $("#lensSize").val('200');
        }

        if ( isNaN(settings.borderThickness) || settings.borderThickness < 0 || settings.borderThickness > 200 ) {
            show_alert('<b>Border Thickness</b> accepts integers between 0 and 200. Your value was reset to 1');
            settings.borderThickness = 1;
            $("#borderThickness").val('1');
        }

        if (isNaN(settings.lensFade) || settings.lensFade < 0 || settings.lensFade > 10000) {
            show_alert('<b>Fade Time</b> accepts integers between 0 and 10. Your value was reset to 1');
            settings.lensFade = 1;
            $("#lensFade").val('1');
        }

        if (isNaN(settings.tintOpacity) || settings.tintOpacity < 0 || settings.tintOpacity > 1) {
            show_alert('<b>Tint Opacity</b> accepts a number between 0 and 1. Your value was reset to 0.5');
            settings.tintOpacity = 0.5;
            $("#tintOpacity").val('0.5');
        }

        if (isNaN(settings.zwWidth) || settings.zwWidth < 0 || settings.zwWidth > 2000) {
            show_alert('<b>Zoom Window Width</b> accepts a number between 0 and 2000. Your value was reset to 400');
            settings.zwWidth = 400;
            $("#zwWidth").val('400');
        }

        if (isNaN(settings.zwHeight) || settings.zwHeight < 0 || settings.zwHeight > 2000) {
            show_alert('<b>Zoom Window Height</b> accepts a number between 0 and 2000. Your value was reset to 360');
            settings.zwHeight = 360;
            $("#zwHeight").val('360');
        }

        if ( isNaN(settings.zwBorderThickness) || settings.zwBorderThickness < 0 || settings.zwBorderThickness > 200 ) {
            show_alert('<b>Border Thickness</b> accepts integers between 0 and 200. Your value was reset to 4');
            settings.zwBorderThickness = 4;
            $("#zwBorderThickness").val('4');
        }

        if ( isNaN(settings.zwBorderRadius) || settings.zwBorderRadius < 0 || settings.zwBorderRadius > 500 ) {
            show_alert('<b>Rounded Corners</b> accepts integers between 0 and 500. Your value was reset to 0');
            settings.zwBorderRadius = 0;
            $("#zwBorderRadius").val('0');
        }

        if ( isNaN(settings.zwFade) || settings.zwFade < 0 || settings.zwFade > 10000 ) {
            show_alert('<b>Fade Time</b> accepts integers between 0 and 10. Your value was reset to 0');
            settings.zwFade = 0;
            $("#zwFade").val('0');
        }

        if ( isNaN(settings.customTextSize) || settings.customTextSize < 0 || settings.customTextSize > 45 ) {
            show_alert('<b>Text Size</b> accepts integers between 0 and 45. Your value was reset to 12');
            settings.customTextSize = 12;
            $("#customTextSize").val('12');
        }


        if ( isNaN(settings.zwResponsiveThreshold) || settings.zwResponsiveThreshold < 1 || settings.zwResponsiveThreshold > 3000 ) {
            show_alert('<b>Responsive Threshold</b> accepts integers between 1 and 3000. Your value was reset to 800');
            settings.zwResponsiveThreshold = 800;
            $("#zwResponsiveThreshold").val('800');
        }

    }

    function enableDisableFields(settings) {
        // activate all fields
        $("#tab_lens, #tab_zoom_window").removeClass('disabled');
        $("#tab_lens a").attr('href', '#lens_settings');
        $("#tab_zoom_window a").attr('href', '#zoom_window_settings');
        $("#lensSize").removeAttr('disabled');
        $("#lensColour").removeAttr('disabled');
        $("#lensBgImage").removeAttr('disabled');
        $("#tintColor").removeAttr('disabled');
        $("#tintOpacity").removeAttr('disabled');
        $("#lensColour").removeAttr('disabled');
        $("#lensOverlay").removeAttr('disabled');
        $("#zwResponsiveThreshold").removeAttr('disabled');

        // Enable/disable fields
        switch ( settings['lensShape'] ) {
            case 'none' :
                $("#tab_lens, #tab_zoom_window").addClass('disabled');
                $("#tab_lens a").attr('href', '');
                $("#tab_zoom_window a").attr('href', '');
                $("#lensColour").attr('disabled', 'disabled');
                $("#lensBgImage").attr('disabled', 'disabled');
                break;
            case 'square' :
            case 'round' :
                $("#tab_zoom_window").addClass('disabled');
                $("#tab_zoom_window a").attr('href', '');
                $("#lensColour").attr('disabled', 'disabled');
                $("#lensBgImage").attr('disabled', 'disabled');
                break;
            case 'zoom_window' :
                $("#lensSize").attr('disabled', 'disabled');
                break;
        }

        if (settings.tint === false) {
            $("#tintColor").attr('disabled', 'disabled');
            $("#tintOpacity").attr('disabled', 'disabled');
        } else {
            $("#lensColour").attr('disabled', 'disabled');
            $("#lensOverlay").attr('disabled', 'disabled');
        }

        if ( settings.zwResponsive === false ) {
            $("#zwResponsiveThreshold").attr('disabled', 'disabled');
        }

    }

    $(".form-group input").change(updateZoomer);


    function show_alert( message ) {
        $("#alert_messages").html( 
            '<div class="alert alert-dismissable alert-danger">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            message +
            '</div>');
    }

});
