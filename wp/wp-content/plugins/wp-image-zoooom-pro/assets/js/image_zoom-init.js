(function(d){d.Observe={}})(jQuery);
(function(d,q){var r=function(e,f){f||(f=e,e=window.document);var m=[];d(f).each(function(){for(var l=[],g=d(this),h=g.parent();h.length&&!g.is(e);h=h.parent()){var f=g.get(0).tagName.toLowerCase();l.push(f+":eq("+h.children(f).index(g)+")");g=h}(h.length||g.is(e))&&m.push("> "+l.reverse().join(" > "))});return m.join(", ")};q.path={get:r,capture:function(e,f){f||(f=e,e=window.document);var m=[];d(f).each(function(){var l=-1,g=this;if(this instanceof Text)for(var g=this.parentNode,h=g.childNodes,
f=0;f<h.length;f++)if(h[f]===this){l=f;break}var k=r(e,g),n=d(e).is(g);m.push(function(e){e=n?e:d(e).find(k);return-1===l?e:e.contents()[l]})});return function(e){e=e||window.document;return m.reduce(function(d,f){return d.add(f(e))},d([]))}}}})(jQuery,jQuery.Observe);(function(d,q){var r=function(e){this.original=d(e);this.root=this.original.clone(!1,!0)};r.prototype.find=function(d){return q.path.capture(this.original,d)(this.root)};q.Branch=r})(jQuery,jQuery.Observe);
(function(d,q){var r=function(a,b){var c={};a.forEach(function(a){(a=b(a))&&(c[a[0]]=a[1])});return c},e=r("childList attributes characterData subtree attributeOldValue characterDataOldValue attributeFilter".split(" "),function(a){return[a.toLowerCase(),a]}),f=r(Object.keys(e),function(a){if("attributefilter"!==a)return[e[a],!0]}),m=r(["added","removed"],function(a){return[a.toLowerCase(),a]}),l=d([]),g=function(a){if("object"===typeof a)return a;a=a.split(/\s+/);var b={};a.forEach(function(a){a=
a.toLowerCase();if(!e[a]&&!m[a])throw Error("Unknown option "+a);b[e[a]||m[a]]=!0});return b},h=function(a){return"["+Object.keys(a).sort().reduce(function(b,c){var d=a[c]&&"object"===typeof a[c]?h(a[c]):a[c];return b+"["+JSON.stringify(c)+":"+d+"]"},"")+"]"},t=window.MutationObserver||window.WebKitMutationObserver,k=function(a,b,c,s){this._originalOptions=d.extend({},b);b=d.extend({},b);this.attributeFilter=b.attributeFilter;delete b.attributeFilter;c&&(b.subtree=!0);b.childList&&(b.added=!0,b.removed=
!0);if(b.added||b.removed)b.childList=!0;this.target=d(a);this.options=b;this.selector=c;this.handler=s};k.prototype.is=function(a,b,c){return h(this._originalOptions)===h(a)&&this.selector===b&&this.handler===c};k.prototype.match=function(a){var b=this.options,c=a.type;if(!this.options[c])return l;if(this.selector)switch(c){case "attributes":if(!this._matchAttributeFilter(a))break;case "characterData":return this._matchAttributesAndCharacterData(a);case "childList":if(a.addedNodes&&a.addedNodes.length&&
b.added&&(c=this._matchAddedNodes(a),c.length))return c;if(a.removedNodes&&a.removedNodes.length&&b.removed)return this._matchRemovedNodes(a)}else{var s=a.target instanceof Text?d(a.target).parent():d(a.target);if(!b.subtree&&s.get(0)!==this.target.get(0))return l;switch(c){case "attributes":if(!this._matchAttributeFilter(a))break;case "characterData":return this.target;case "childList":if(a.addedNodes&&a.addedNodes.length&&b.added||a.removedNodes&&a.removedNodes.length&&b.removed)return this.target}}return l};
k.prototype._matchAttributesAndCharacterData=function(a){return this._matchSelector(this.target,[a.target])};k.prototype._matchAddedNodes=function(a){return this._matchSelector(this.target,a.addedNodes)};k.prototype._matchRemovedNodes=function(a){var b=new q.Branch(this.target),c=Array.prototype.slice.call(a.removedNodes).map(function(a){return a.cloneNode(!0)});a.previousSibling?b.find(a.previousSibling).after(c):a.nextSibling?b.find(a.nextSibling).before(c):(this.target===a.target?b.root:b.find(a.target)).empty().append(c);
return this._matchSelector(b.root,c).length?d(a.target):l};k.prototype._matchSelector=function(a,b){var c=a.find(this.selector);b=Array.prototype.slice.call(b);return c=c.filter(function(){var a=this;return b.some(function(b){return b instanceof Text?b.parentNode===a:b===a||d(b).has(a).length})})};k.prototype._matchAttributeFilter=function(a){return this.attributeFilter&&this.attributeFilter.length?0<=this.attributeFilter.indexOf(a.attributeName):!0};var n=function(a){this.patterns=[];this._target=
a;this._observer=null};n.prototype.observe=function(a,b,c){var d=this;this._observer?this._observer.disconnect():this._observer=new t(function(a){a.forEach(function(a){d.patterns.forEach(function(b){var c=b.match(a);c.length&&c.each(function(){b.handler.call(this,a)})})})});this.patterns.push(new k(this._target,a,b,c));this._observer.observe(this._target,this._collapseOptions())};n.prototype.disconnect=function(a,b,c){var d=this;this._observer&&(this.patterns.filter(function(d){return d.is(a,b,c)}).forEach(function(a){a=
d.patterns.indexOf(a);d.patterns.splice(a,1)}),this.patterns.length||this._observer.disconnect())};n.prototype.disconnectAll=function(){this._observer&&(this.patterns=[],this._observer.disconnect())};n.prototype.pause=function(){this._observer&&this._observer.disconnect()};n.prototype.resume=function(){this._observer&&this._observer.observe(this._target,this._collapseOptions())};n.prototype._collapseOptions=function(){var a={};this.patterns.forEach(function(b){var c=a.attributes&&a.attributeFilter;
if(!c&&a.attributes||!b.attributeFilter)c&&b.options.attributes&&!b.attributeFilter&&delete a.attributeFilter;else{var e={},f=[];(a.attributeFilter||[]).concat(b.attributeFilter).forEach(function(a){e[a]||(f.push(a),e[a]=1)});a.attributeFilter=f}d.extend(a,b.options)});Object.keys(m).forEach(function(b){delete a[m[b]]});return a};var p=function(a){this.patterns=[];this._paused=!1;this._target=a;this._events={};this._handler=this._handler.bind(this)};p.prototype.NS=".jQueryObserve";p.prototype.observe=
function(a,b,c){a=new k(this._target,a,b,c);d(this._target);a.options.childList&&(this._addEvent("DOMNodeInserted"),this._addEvent("DOMNodeRemoved"));a.options.attributes&&this._addEvent("DOMAttrModified");a.options.characterData&&this._addEvent("DOMCharacerDataModified");this.patterns.push(a)};p.prototype.disconnect=function(a,b,c){var e=d(this._target),f=this;this.patterns.filter(function(d){return d.is(a,b,c)}).forEach(function(a){a=f.patterns.indexOf(a);f.patterns.splice(a,1)});var g=this.patterns.reduce(function(a,
b){b.options.childList&&(a.DOMNodeInserted=!0,a.DOMNodeRemoved=!0);b.options.attributes&&(a.DOMAttrModified=!0);b.options.characterData&&(a.DOMCharacerDataModified=!0);return a},{});Object.keys(this._events).forEach(function(a){g[a]||(delete f._events[a],e.off(a+f.NS,f._handler))})};p.prototype.disconnectAll=function(){var a=d(this._target),b;for(b in this._events)a.off(b+this.NS,this._handler);this._events={};this.patterns=[]};p.prototype.pause=function(){this._paused=!0};p.prototype.resume=function(){this._paused=
!1};p.prototype._handler=function(a){if(!this._paused){var b={type:null,target:null,addedNodes:null,removedNodes:null,previousSibling:null,nextSibling:null,attributeName:null,attributeNamespace:null,oldValue:null};switch(a.type){case "DOMAttrModified":b.type="attributes";b.target=a.target;b.attributeName=a.attrName;b.oldValue=a.prevValue;break;case "DOMCharacerDataModified":b.type="characterData";b.target=d(a.target).parent().get(0);b.attributeName=a.attrName;b.oldValue=a.prevValue;break;case "DOMNodeInserted":b.type=
"childList";b.target=a.relatedNode;b.addedNodes=[a.target];b.removedNodes=[];break;case "DOMNodeRemoved":b.type="childList",b.target=a.relatedNode,b.addedNodes=[],b.removedNodes=[a.target]}for(a=0;a<this.patterns.length;a++){var c=this.patterns[a],e=c.match(b);e.length&&e.each(function(){c.handler.call(this,b)})}}};p.prototype._addEvent=function(a){this._events[a]||(d(this._target).on(a+this.NS,this._handler),this._events[a]=!0)};q.Pattern=k;q.MutationObserver=n;q.DOMEventObserver=p;d.fn.observe=
function(a,b,c){b?c||(c=b,b=null):(c=a,a=f);return this.each(function(){var e=d(this),f=e.data("observer");f||(f=t?new n(this):new p(this),e.data("observer",f));a=g(a);f.observe(a,b,c)})};d.fn.disconnect=function(a,b,c){a&&(b?c||(c=b,b=null):(c=a,a=f));return this.each(function(){var e=d(this),f=e.data("observer");f&&(a?(a=g(a),f.disconnect(a,b,c)):(f.disconnectAll(),e.removeData("observer")))})}})(jQuery,jQuery.Observe);

!function(a){function b(){var a=document.createElement("p"),b=!1;if(a.addEventListener)a.addEventListener("DOMAttrModified",function(){b=!0},!1);else{if(!a.attachEvent)return!1;a.attachEvent("onDOMAttrModified",function(){b=!0})}return a.setAttribute("id","target"),b}function c(b,c){if(b){var d=this.data("attr-old-value");if(c.attributeName.indexOf("style")>=0){d.style||(d.style={});var e=c.attributeName.split(".");c.attributeName=e[0],c.oldValue=d.style[e[1]],c.newValue=e[1]+":"+this.prop("style")[a.camelCase(e[1])],d.style[e[1]]=c.newValue}else c.oldValue=d[c.attributeName],c.newValue=this.attr(c.attributeName),d[c.attributeName]=c.newValue;this.data("attr-old-value",d)}}var d=window.MutationObserver||window.WebKitMutationObserver;a.fn.attrchange=function(e,f){if("object"==typeof e){var g={trackValues:!1,callback:a.noop};if("function"==typeof e?g.callback=e:a.extend(g,e),g.trackValues&&this.each(function(b,c){for(var d,e={},f=0,g=c.attributes,h=g.length;h>f;f++)d=g.item(f),e[d.nodeName]=d.value;a(this).data("attr-old-value",e)}),d){var h={subtree:!1,attributes:!0,attributeOldValue:g.trackValues},i=new d(function(b){b.forEach(function(b){var c=b.target;g.trackValues&&(b.newValue=a(c).attr(b.attributeName)),"connected"===a(c).data("attrchange-status")&&g.callback.call(c,b)})});return this.data("attrchange-method","Mutation Observer").data("attrchange-status","connected").data("attrchange-obs",i).each(function(){i.observe(this,h)})}return b()?this.data("attrchange-method","DOMAttrModified").data("attrchange-status","connected").on("DOMAttrModified",function(b){b.originalEvent&&(b=b.originalEvent),b.attributeName=b.attrName,b.oldValue=b.prevValue,"connected"===a(this).data("attrchange-status")&&g.callback.call(this,b)}):"onpropertychange"in document.body?this.data("attrchange-method","propertychange").data("attrchange-status","connected").on("propertychange",function(b){b.attributeName=window.event.propertyName,c.call(a(this),g.trackValues,b),"connected"===a(this).data("attrchange-status")&&g.callback.call(this,b)}):this}return"string"==typeof e&&a.fn.attrchange.hasOwnProperty("extensions")&&a.fn.attrchange.extensions.hasOwnProperty(e)?a.fn.attrchange.extensions[e].call(this,f):void 0}}(jQuery);

jQuery(window).ready(function($){
    var options = IZ.options; 

    // Fix for the Lazy Load plugin with jQuery.sonar
    $("img[data-lazy-src]").each(function(){
        $(this).attr('data-zoom-image', $(this).data('lazy-src'));
    });

    // Get the image url from data-large_image
    $("img[data-large_image]").each(function(){
        $(this).attr('data-zoom-image', $(this).data('large_image'));
    });



    var images = "img.zoooom, .zoooom img, .mp_product_image_single, a.zoomGallery img, .portfolio_images img, .single-fluxus_portfolio .project-image img, .attachment-product_page_image, .product-slider-image, .slider-content-images img";
    $( images ).image_zoom( options );

    var attrchange_zoom = {
        trackValues: true,
        callback: function(event) {
            if ( event.newValue == event.oldValue ) return;
            if ( event.attributeName == 'href' ) {
                $(this).attr('src', event.newValue );
            }
            if ( event.attributeName == 'src' ) {
                $(".zoomContainer").remove();
                $(this).image_zoom( options );
            }
        }
    }; 

    $( "img.zoooom, a.zoomGallery").attrchange( attrchange_zoom );

    // Custom class or id selector 
    if ( IZ.custom_class != '' ) {
        $( IZ.custom_class ).image_zoom(options);
    }

    // WooCommerce category pages
    if ( IZ.woo_categories == '1' ) {
        $(".tax-product_cat .products img").image_zoom(options);
    }

    // Force attachments
    if ( IZ.force_attachments == '1' ) {
        $(".type-attachment img").image_zoom(options);
    }

    // Show zoom for lazy_load images
    if (typeof $.unveil === "function") { 
        $("img.unveil").unveil(0, function() {
            $(this).load(function() {
                $("img.zoooom").image_zoom(options);
                $(".zoooom img").image_zoom(options);
            });
        });
    }


    // Resize the zoom windows when resizing the page
    $(window).bind('resize', function(e) {
        window.resizeEvt;
        $(window).resize(function() {
            clearTimeout(window.resizeEvt);
            window.resizeEvt = setTimeout(function() {
                $(".zoomContainer").remove();
                $( images ).image_zoom(options);
            }, 500);
        });
    });


    // Compatibility with the Flexslider plugin
    if ( IZ.flexslider.length > 0 ) {
        if ( $(IZ.flexslider + " .slides").length > 0 ) {
            setTimeout( function() {
                $(IZ.flexslider + " img").first().image_zoom( options );
            }, 500 );

            var flexslider_counter = 0;
            var old_value = "";
            $(IZ.flexslider + " .slides").attrchange({
                trackValues: true,
                callback: function(event) {
                    if ( event.newValue != old_value ) {
                        $(".zoomContainer").remove();
                        setTimeout( function() {
                            $(IZ.flexslider + " .flex-active-slide img").image_zoom( options );
                        }, 400);
                    }
                    old_value = event.newValue;
                }
            });
        } 
    }



    if ( IZ.with_woocommerce == '1' ) {
        if ( IZ.flexslider.length === 0 ) {
            if ( $(".attachment-shop_single").length > 0 ) {
                $(".attachment-shop_single").image_zoom(options);
            }

            $( ".attachment-shop_single").attrchange( attrchange_zoom );
        }

        if ( IZ.remove_lightbox_thumbnails != '0' || IZ.remove_lightbox != '0' ) {
        $("a[data-rel^='zoomImage']").each(function(index){
            $(this).click(function(event){
                // If there are more than one WooCommerce gallery, exchange the thumbnail with the closest .attachment-shop_single
                var obj1 = $(".attachment-shop_single");
                if ( obj1.length > 1 ) {
                    var obj1 = $(this).closest('.images').find( $(".attachment-shop_single") );
                }
                var obj2 = $(this).find("img");

                event.preventDefault();

                if ( obj2.hasClass('attachment-shop_single') === false ) {

                    // Remove the srcset and sizes
                    obj1.removeAttr('srcset').removeAttr('sizes');
                    obj2.removeAttr('srcset').removeAttr('sizes');

                    var thumb_src = obj2.attr('src');

                    // Exchange the attributes
                    $.each(['alt', 'title'], function(key,attr) {
                        var temp;
                        if ( obj1.attr( attr ) ) temp = obj1.attr( attr ); 
                        if ( obj2.attr( attr ) ) {
                            obj1.attr(attr, obj2.attr(attr) );
                        } else {
                            obj1.removeAttr( attr );
                        }
                        if ( IZ.exchange_thumbnails == '1' ) {
                            if ( temp && temp.length > 0 ) {
                                obj2.attr(attr, temp);
                            } else {
                                obj2.removeAttr( attr );
                            }
                        }
                    });

                    // Exchange the link sources
                    var temp;
                    temp = obj2.parent().attr('href');
                    if ( IZ.exchange_thumbnails == '1' ) {
                        obj2.parent().attr('href', obj1.parent().attr('href'));
                    }
                    obj1.parent().attr('href', temp );

                    // Set the obj1.src = the link source
                    obj1.attr('src', temp ); 

                    // Set the obj2.src = data-thumbnail-src
                    if ( obj1.data('thumbnail-src') && IZ.exchange_thumbnails == '1' ) {
                        obj2.attr( 'src', obj1.attr('data-thumbnail-src'));
                    }

                    // Set the obj1.data-thumbnail-src
                    obj1.attr('data-thumbnail-src', thumb_src ); 

                    // Replace the data-zoom-image
                    temp = obj1.data('zoom-image');
                    if ( !obj2.data('zoom-image') ) obj2.data('zoom-image', ''); 
                    obj1.data('zoom-image', obj2.data('zoom-image'));
                    if( ! temp ) temp = '';
                    obj2.data('zoom-image', temp);


                    // Remove the old zoom and reactive the new zoom
                    $(".zoomContainer").remove();
                    $(".attachment-shop_single").image_zoom(options);
                }

            });
        });
        }

        
        // add the prettyPhoto only on the main image
        if ( IZ.remove_lightbox_thumbnails == '1' && IZ.remove_lightbox == '0' ) {
            if( typeof $({}).prettyPhoto == 'function') {
                $(".thumbnails a").click( function(e ) {
                    e.preventDefault();
                    unbind('click.prettyphoto');
                });

                $("a[data-rel^='zoomImage']").prettyPhoto({
                    hook: 'data-rel',
                    social_tools: false,
                    theme: 'pp_woocommerce',
                    horizontal_padding: 20,
                    opacity: 0.8,
                    deeplinking: true,
                    overlay_gallery: true
                });
            }
        }

    }


    // Show zoom on the WooCommerce 3.0.+ gallery with slider
    if ( IZ.with_woocommerce == '1' && (IZ.woo_slider == '1' || $('.woo_product_slider_enabled').length > 0 )) {
        if ( $(".woocommerce-product-gallery img").length > 0 ) {
            options.onMouseMove = true;
            var first_img = ".woocommerce-product-gallery__wrapper img";
            setTimeout( function() {
                if ( $(".flex-viewport").length > 0 ) {
                    first_img = ".woocommerce-product-gallery__wrapper .flex-active-slide img";
                } 
                $(first_img).first().image_zoom( options );
            }, 500 );

            var flexslider_counter = 0;
            var old_value = "";
            $(".woocommerce-product-gallery__wrapper").attrchange({
                trackValues: true,
                callback: function(event) {
                    if ( event.newValue != old_value ) {
                        $(".zoomContainer").remove();
                        setTimeout( function() {
                            $(first_img).first().image_zoom(options);
                        }, 550);
                    }
                    old_value = event.newValue;
                }
            });

            // Resize the zoom windows when resizing the page
            $(window).bind('resize', function(e) {
                window.resizeEvt;
                $(window).resize(function() {
                    clearTimeout(window.resizeEvt);
                    window.resizeEvt = setTimeout(function() {
                        $(".zoomContainer").remove();
                        $(first_img).first().image_zoom(options);
                    }, 300);
                });
            });

            if ( IZ.remove_lightbox == '1' ) {
                $(".woocommerce-product-gallery img").click(function(e){
                    e.preventDefault();
                });
            }

            // Change the zoom for product variations
            $(".attachment-shop_thumbnail").attrchange({
                trackValue: true,
                callback: function(event) {
                    if(event.attributeName == 'data-large_image' ) {
                        $(this).data('zoom-image', $(this).attr('data-large_image') ); 
                    }
                }
            });

        } 
    }




    // Show zoom on the WooCommerce 3.0.+ gallery without slider
    if ( IZ.with_woocommerce == '1' && (IZ.woo_slider == '0' || $('.woo_product_slider_disabled').length > 0)) { 
        var first_img = $('.woocommerce-product-gallery__image:first-child img');

        // Zoom on the first image
        first_img.image_zoom(options);

        // Remove the click action on the images
        $('.woocommerce-product-gallery__image img').click(function(e){
            e.preventDefault();
        });


        $('.woocommerce-product-gallery__image img').each(function(i) {
            $(this).removeAttr('data-large_image');
            $(this).removeAttr('data-large_image_width');
            $(this).removeAttr('data-large_image_height');
            $(this).removeAttr('srcset');
            $(this).removeAttr('sizes');
        });

        // Switch the thumbnail with the main image
        $(".woocommerce-product-gallery__image:nth-child(n+2) img").each(function(i){
            $(this).click(function(e){
                var this_thumb = $(this);
                // Exchange the attributes
                $.each(['alt', 'title', 'data-src'], function(key,attr) {
                    var temp;
                    if ( first_img.attr( attr ) ) temp = first_img.attr( attr ); 
                    if ( this_thumb.attr( attr ) ) {
                        first_img.attr(attr, this_thumb.attr(attr) );
                    } else {
                        first_img.removeAttr( attr );
                    }
                    if ( IZ.exchange_thumbnails == '1' ) {
                        if ( temp && temp.length > 0 ) {
                            this_thumb.attr(attr, temp);
                        } else {
                            this_thumb.removeAttr( attr );
                        }
                    }

                });

                var thumb_src = this_thumb.attr('src');


                // Exchange the link sources
                var temp;
                temp = this_thumb.parent().attr('href');
                if ( IZ.exchange_thumbnails == '1' ) {
                    this_thumb.parent().attr('href', first_img.parent().attr('href'));
                }
                first_img.parent().attr('href', temp );

                // Set the first_img.src = the link source
                first_img.attr('src', temp ); 

                // Set the this_thumb.src = data-thumbnail-src
                if ( first_img.data('thumbnail-src') && IZ.exchange_thumbnails == '1' ) {
                    this_thumb.attr( 'src', first_img.attr('data-thumbnail-src'));
                }

                // Set the first_img.data-thumbnail-src
                first_img.attr('data-thumbnail-src', thumb_src ); 

                // Replace the data-zoom-image
                temp = first_img.data('zoom-image');
                if ( !this_thumb.data('zoom-image') ) this_thumb.data('zoom-image', ''); 
                first_img.data('zoom-image', this_thumb.data('zoom-image'));
                if( ! temp ) temp = '';
                this_thumb.data('zoom-image', temp);

                // Remove the old zoom and reactive the new zoom
                $(".zoomContainer").remove();
                first_img.image_zoom(options);

            });
        });


        if ( IZ.remove_lightbox == '1' ) {
            $(".woocommerce-product-gallery img").click(function(e){
                e.preventDefault();
            });
        }
    }





    // compatibility with Fancybox
    if ( IZ.enable_fancybox === '1' ) {
    setTimeout( function() {
       if ( $("#fancybox-content").length > 0 ) {
           $("#fancybox-close").attrchange({
               trackValues: true,
               callback: function( event ) {
                   if ( event.newValue == event.oldValue ) return;
                   if ( event.newValue == 'display: none;' ) {
                        $('#fancybox-content img').remove();
                        $(".zoomContainer").remove();
                        return;
                   } 
                   if ( $("#fancybox-content img").length > 0 ) {
                       options.zIndex = 112400;
                       options.onMouseMove = true;
                       $('#fancybox-left').remove();
                       $('#fancybox-right').remove();
                       $("#fancybox-content img").image_zoom(options);
                   }
               }
           });
       }
    }, 500);

    // for older versions from Fancybox, for example version 2.1.3 from 23 Oct 2012
    $('body').observe('added', '.fancybox-inner img', function(r){
        $(".fancybox-inner img").image_zoom(options);
        $(".fancybox-overlay").attrchange({
            trackValues: true,
            callback: function( event ) {
                if ( event.newValue == event.oldValue ) return;
                    $('.fancybox-inner img').remove();
                    $(".zoomContainer").remove();
                return;
            }
        });
    });
    }


    // compatibility with "Search & Filter PRO" from "Designs & Code"
    if ( IZ.search_filter_pro === '1' ) {
        $(document).on("sf:ajaxfinish", ".searchandfilter", function(){
            $(".zoomContainer").remove();
            $(".attachment-shop_single").image_zoom(options);
            $(".zoooom").image_zoom(options);
        });
    }

    // compatibility with the Huge IT Gallery
    if ( IZ.huge_it_gallery.length > 0 ) {
        if( IZ.huge_it_gallery.indexOf(',') >= 0 ) {
            IZ.huge_it_gallery.split(',').forEach(function(id){
                id = id.trim();
                $("#huge_it_gallery_container_moving_"+ id +" img").image_zoom(options);
            });
        } else {
            var id = IZ.huge_it_gallery.trim();
            $("#huge_it_gallery_container_moving_"+ id +" img").image_zoom(options);
        }
        $(".gallery-image-overlay").remove();

        // compatibility with the Huge IT Gallery lightbox
        $('body').observe('added', 'img.gicboxPhoto', function(r){
            options.zIndex = 19999;
            $('img.gicboxPhoto').image_zoom( options );
            $("#gicolorbox").attrchange({
                trackValues: true,
                callback: function(event) {
                    if ( event.newValue == event.oldValue ) return;
                    if ( event.attributeName == 'style' ) {
                        if ( event.newValue.indexOf('opacity') !== -1 ) {
                            $(".zoomContainer").remove();
                        }
                    }
                }
            });
        });

        // compatibility with the Huge IT Gallery Lightbox new
        $('body').observe('added', '.rwd-container', function(r){
           // TODO 
        });
    }

    // compatibility with the Flatsome theme gallery
    if ( $(".slide.easyzoom img").length > 0 ) {
        $(".slide.easyzoom img").each(function(img){
            var a_href = $(this).parent().attr('href');
            $(this).attr('data-zoom-image', a_href );
        });
        $(".slide.easyzoom img").image_zoom( options );
    }


    // compatibility with Jetpack Carousel (inside the lightbox)
    if ( IZ.enable_jetpack_carousel === '1' ) {
        options.zIndex = 2177483647;

        // Start zoom when the carousel is opened on click
        $( document.body ).on( 'click.jp-carousel', 'div.gallery,div.tiled-gallery', function(e) {
            $('.jp-carousel-slide.selected img').image_zoom( options );
        });
        // Start zoom when the carousel is already open on the page
        $( window ).on( 'hashchange.jp-carousel', function () {
            setTimeout( function() {
                $('.jp-carousel-slide.selected img').image_zoom( options );
                // When the slide changes, it also replaces the dummy 1-pixel image with the real image
                $(".jp-carousel-slide img").attrchange({
                    trackValues: true,
                    callback: function(event) {

                        if ( event.newValue == event.oldValue ) return;
                        if ( event.attributeName == 'src' ) {
                            $('.jp-carousel-slide.selected img').image_zoom( options );
                        }
                    }
                });

            }, 500);
        });
    }


    // Compatibility with the Lightbox from Photo Gallery by WebDorado
    if ( IZ.enable_photo_gallery === '1' ) {
        $('body').observe('added', '#spider_popup_wrap', function(r){
            options.zIndex = 300000;
            options.onMouseMove = true;
            $('#spider_popup_wrap .bwg_popup_image').image_zoom(options);

            $('.bwg_popup_image_spun').observe('attributes', function(ra){
                if ( ra.attributeName == 'image_id' ) {
                    $('#spider_popup_wrap .bwg_popup_image').image_zoom(options);
                }
            });

            $('body').observe('removed', '#spider_popup_wrap', function(rb) {
                $(".zoomContainer").remove();
            });
        });
    }


    if ( IZ.enable_fancybox === '1' ) {
        // Compatibility with the iLightbox on the Avada Portfolio
        $('body').observe('added', '.ilightbox-overlay', function(r){
            setTimeout(function() {
                options.zIndex = 110003;
                options.onMouseMove = true;
                $('img.ilightbox-image').image_zoom(options);

                $('body').observe('added', 'img.ilightbox-image', function(ra){
                    setTimeout(function() {
                        $('img.ilightbox-image').image_zoom(options);
                    }, 500);
                });
            }, 500);

            $('body').observe('removed', '.ilightbox-overlay', function(rb) {
                $(".zoomContainer").remove();
            });

        });

        // Compatibility with the prettyPhoto lightbox (also used by the Visual Composer gallery)
        $('body').observe('added', '#fullResImage', function(r){
            setTimeout(function() {
                $('.pp_hoverContainer').remove();
                $('.pp_expand').remove();
                options.zIndex = 110003;
                options.onMouseMove = true;
                $('#fullResImage').image_zoom(options);
            }, 300);

            $('body').observe('removed', '.pp_pic_holder', function(rb) {
                $(".zoomContainer").remove();
            });
        });
    }


   // Compatibility with the Shopkeeper's theme WooCommerce gallery
   if ( typeof IZ.shopkeeper != 'undefined' && IZ.shopkeeper == '1')  {
       if ( $(".swiper-slide-active img").length > 0 ) {
           $(".swiper-slide").children().removeClass('easyzoom').removeClass('el_zoom');
           $(".swiper-slide-active img").image_zoom(options);
           var change_timestamp = 0;
           $(".swiper-wrapper").attrchange({
               trackValues: true,
               callback: function( event ) {
                    if ( Date.now() - change_timestamp < 200 ) return;
                    change_timestamp = Date.now();
                    $(".zoomContainer").remove();
                    setTimeout(function() {
                        $(".swiper-slide-active img").image_zoom(options);
                    }, 400);
               }
           });
       }
    }



});
