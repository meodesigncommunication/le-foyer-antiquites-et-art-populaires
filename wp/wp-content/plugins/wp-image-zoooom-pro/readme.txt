=== WP Image Zoom PRO ===
Created: 27/05/2015
Contributors: Diana Burduja 
Tags: image, zoom, woocommerce, image zoom, magnifier, image magnifier, product image, no lightbox 
Requires at least: 3.0.1
Tested up to: 4.7
Stable tag: 1.21
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Awesome image zoom plugin for images in posts/pages and for WooCommerce products.

== Description ==

= Description =

Are you looking for a robust, modern and very configurable image zoom plugin? WP Image Zoooom will allow you easily to create a magnifying glass on your images, all from a very intuitive WP admin interface.

Allow your visitors to see the details of your images. Improve the user experience. Improve your revenue.

= Our users love these features =

* **4 zooming types** - inner zoom, round lens, square lens and outer zoom.
* **Animation Easing Effect** - the zooming lense will follow the mouse over the image with a sleak delay. This will add a touch of elegance to the zooming in experience.
* **Fade Effect** - the zoomed part will gracefully fade in or fade away.
* **Responsive** - the zoom window will adapt to the browser's width. 
* **Extremely configurable** - control zooming lens size, border color, border size, shadow, rounded corner, and others ...
* **Works with WooCommerce** - easily enable the zoom on all your products' images. Only a checkbox away.
* **Works in Pages and Posts** - within the post's/page's editor you'll find a button for applying the zooming effect on any image.

== Installation ==

* Download the plugin.
* Save the .zip file to a location on your computer.
* Open the WP admin panel, and click "Plugins" -> "Add new".
* Click "upload".. then browse to the downloaded .zip file.
* Click "Install".. and then "Activate plugin".

OR...

* Download the plugin.
* Extract the .zip file to a location on your computer.
* Use either FTP or your hosts cPanel to gain access to your website file directories.
* Browse to the `wp-content/plugins` directory.
* Upload the extracted `wp-image-zoooom-pro` folder to this directory location.
* Open the WP admin panel.. click the "Plugins" page.. and click "Activate" under the newly added "WP Image Zoooom PRO" plugin.

== Frequently Asked Questions ==

= Does it work with W3 Total Cache? =
Yes

= If I have Visual Composer installed, how do I apply the zoom? =
You can apply the zoom on the Single Image element from Visual Composer. See the following [screenshot](https://s.w.org/plugins/wp-image-zoooom/screenshot-7.jpg).

= It display the zoom lens, but the picture is not enlarged =
In order for the zoom to work you have to upload a bigger picture than the one presented on the website. This is how all the zoom plugins work, there is no workaround to that.

= The zoom window is about 1cm lower than the zoom window =
This is an effect caused by the WordPres Admin Bar. Try logging out and check the zoom again.

= How to zoom an image without the button in the editor? =
When you add a CSS class called 'zoooom' to any image, the zoom will be applied on that particular image. Remember that the zooming works only when the displayed image is smaller than the loaded image (i.e. the image is shrinked with "width" and "height" attributes).

= If I want to use a "lazy load" plugin will it work? =
We can ensure compatibility with [Unveil Lazy Load](https://wordpress.org/plugins/unveil-lazy-load/), [WP images lazy loading](https://wordpress.org/plugins/wp-images-lazy-loading/) and [Lazy Load](https://wordpress.org/plugins/lazy-load/) plugins. 


= Known Incompatibilities? =

* **Black Studio Tiny MCE Widget** plugin and **SiteOrigin Widgets Bundle** plugin makes the Image Zoooom button doesn't show in the Edit Post and Edit Page editor

* The zoom doesn't work well with **Image Carousel** on **Avada** theme. You cannot use the zoom and the carousel on the same page.

* The zoom doesn't work at all with the **WooCommerce Dynamic Gallery** plugin. 

= Credits =

* Demo photo from http://pixabay.com/en/wordcloud-tagcloud-cloud-text-tag-679951/ under CC0 Public Domain license


== Screenshots ==

1. Configuration menu for the Round Lens

2. Configuration menu for the Square Lens

3. Configuration menu for the Zoom Window

4. Application of zoom on an image in a post

5. General configuration menu

6. WooCommerce product page with the Round Lens applied on the featured image

== Credits ==

* Demo photo from http://pixabay.com/en/wordcloud-tagcloud-cloud-text-tag-679951/ under CC0 Public Domain license

== Changelog ==

= 1.21 =
* Fix: remove var_dump from includes/edd/EDD_SL_Plugin_Updater.php 

= 1.20 =
* Feature: allow beta releases for the plugin
* Fix: initialize the zoom on mousemove instead of mouseover for WooCommerce gallery slider
* Fix: replace the data-zoom-image for WooCommerce variation images

= 1.19 =
* Fix the gallery with GeneratePress theme

= 1.18 =
* Fix: adjust the zoom to the Enfold with WooCommerce 3.0.+
* Feature: compatibility with the ShopKeeper theme
* Feature: compatibility with the Corpobox Lite theme and WooCommerce 3.0.+ 
* Feature: compatibility with the HyperX theme
* Feature: compatibility with the prettyPhoto lightbox

= 1.17 =
* Fix: the slider was skewed on the Divi theme
* Fix: make the "Remove the Lightbox on WooCommerce products" option compatible with the slider
* Fix: if the image has data-large_image attribute, then use that for the zoom
* Fix: if data-zoom-image attribute present, then exchange it with the thumbnails in WooCommerce gallery

= 1.16 =
* Fix: compatibility with WooCommerce 3.0.+
* Fix: Some browsers initialize two lenses for "mouseonmove mouseenter" action

= 1.15 =
* Feature: compatibility with iLightbox, as used in the Avada theme
* Feature: compatibility with the LoveStory theme

= 1.14 =
* Feature: add onMouseMove option for popups and lightboxes, not only onMouseEnter
* Fix: compatibility with an older version of FancyBox
* Fix: set the zoom's z-index higher than Easy FancyBox
* Fix: the left and right arrows from Easy FancyBox were adding a delay to the onmousemove action

= 1.13 = 
* Feature: compatibility with Lazy Load plugin (https://wordpress.org/plugins/lazy-load/)
* Fix: remove the "Compatible with LazyLoad (unveil)" option and apply the fix automatically if the $.unveil function is present
* Change: replace the `move` cursor type with `zoom-in`
* Feature: compatibility with the Nouveau theme
* Fix: rename style ids to avoid conflicts with other plugins

= 1.12 = 
* Fix: image_zoom-init.js was not loaded because the it was missing the jquery-observe dependency

= 1.11 =
* Feature: add filter for loading the jquery-observe.js
* Feature: add zoom on the Huge IT galleries inside the Lightbox
* Feature: add zoom on the Photo Gallery inside the Lightbox

= 1.10 =
* Fix: Add back the `Custom CSS Class` option for Enfold theme builder elements

= 1.9 =
* Fix: don't add the full size image to the srcset if the image is cropped
* Check: compatibility with the WP4.7

= 1.8 =
* Fix: remove warning about "non full size image"
* Fix: compatibility with the Stockholm theme
* Fix: remove warning if the Visual Composer Single Image element is removed

= 1.7 =
* Feature: compatibility with the Jetpack Carousel lightbox
* Fix: trim the input for flexslider container class
* Fix: make the flexslider independent of the WooCommerce setting
* Fix: with W3 Total Cache plugin, under Avada, the JS was minified before jquery.js was loaded

= 1.6 =
* Fix: the zoom on mouse on touchscreen laptops 

= 1.5 =
* Feature: add the "Enable inside fancyBox lightbox" option
* Feature: make the z-index inside the jquery.image-zoom.js class an option
* Fix: match the theme by part of the name

= 1.4 =
* Feature: "Exchange the thumbnail with the main image on WooCommerce      products" option
* Feature: compatibility with the Artcore theme 
* Feature: show a notice about BWP Minify configurations

= 1.3.3 =
* Feature: Add "Apply zoom on this particular image(s)" field in the General Settings

= 1.3.2 =
* Fix: if the full image is not present in the srcset, then add it
* Fix: JavaScript error related to the TinyMCE button and Site Builder by SiteOrigin 

= 1.3.1 =
* Fix: compatibility with Divi theme, Gallery module with Grid layout
* Feature: compatibility with the Storefront ToyStore child theme
* Feature: compatibility with the Flatsome theme product gallery
* Feature: compatibility with all the Enfold elements 

= 1.3.0 =
* Fix: apply the attrchange also on img.zoooom
* Fix: add _nonce to the admin form
* Feature: add an admin warning in case there is Jetpack Photon installed
* Feature: option "Remove the Lightbox on thumbnail images"
* Fix: design on the General Settings page
* Fix: the zoom was jumping on mousewheel zoom

= 1.2.9 =
* Feature: add right-center and left-center position for the Zoom Image
* Fix: updated the compatiblity with the `show-slides` plugin 
* Fix: the license implementation was conflicting with other plugins with EDD license

= 1.2.8 =
* Feature: compatibility with the Aside theme
* Feature: add data-zoom-image attribute if the srcset is not present, but the "zoooom" class is present
* Fix: for zoom use the biggest image from the srcset, not the last one
* Check: compatibility with WP 4.5.1

= 1.2.7 =
* Feature: apply the zoom on more than one WooCommerce gallery on a page
* Fix: when the thumbnails are clicked, change also the link's href
* Fix: when the thumbnails are clicked and an attribute is missing, remove the attribute from the replaced image
* Fix: when the thumbnails are clicked and srcset is missing, try to force the bigger image 
* Fix: the zoom was reading the image's "zoom-image" attribute instead of "data-zoom-image"
* Fix: "Enable the zoom on click" and "Zoom Level" didn't have immediate effect on the image in the admin

= 1.2.6 =
* Feature: use the biggest image from srcset instead of tweaking the image from src
* Fix: update to the WooCommerce 4.5.2 changes to the variation products
* Fix: don't create multiple onclick and onmouseenter events
* Fix: on click change all the attributes of the WooCommerce gallery

= 1.2.5 =
* Feature: enable the zoom on click
* Fix: show the JS code for WooCommerce even if it's on a product's page
* Feature: zoom even if the image is smaller than presented
* Feature: Control the zoom level
* Fix: the zoomed area was misplaced when the image had a padding

= 1.2.4 =
* Feature: compatibility with Stocky theme (EDD )
* Fix: when "Force it to work with WooCommerce" is disabled the srcset is not removed
* Feature: compatibility with "Search & Filter PRO" from "Designs & Code"
* Feature: you can tag a div with "zoooom" class in order to apply the zoom
* Feature: compatibility with Visual Composer 
* Fix: replaced the <?= ?> with <?php echo ?> to make it work for PHP < 5.4 and short_open_tag = Off
* Feature: Enable the zoom on the WooCommerce category pages

= 1.2.3 =
* Feature: compatibility with Huge IT Gallery
* Feature: compatibility with Bridge Portfolio images
* Fix: the zoom was not working on Apple touchscreen devices

= 1.2.2 =
* Feature: compatibility with http://flexslider.woothemes.com/
* Fix: if the image is not resized by WP, but with the help of ?resize=width/height, then if couldn't load the large image
* Feature: add option for "Remove the Lightbox"

= 1.2.1 =
* Fix: TinyMCE editor in WP-Lister Templates was not working
* Fix: With WordPress 4.4 the WooCommerce thumbnail images were not switched for the main image

= 1.2.0 =
* New feature: `Lens Color` and `Show Lens as Grid`
* New feature: `Responsive` and `Responsive Threshold` for the Zooom Window
* Compatibility: `The 7` gallery shortcode
* Fix: Mousewheel Zoom was not working
* Fix: the tooltips were not always showing
* Fix: when changing the image for WooCommerce variable product, force it to load the bigger image

= 1.1.5 =
* Added option to enable the zoom on attachment pages

= 1.1.4 =
* Fix: The zoom window positioning was not working on the front side, only on the backside
* Fix: with round and square lens the zoom was flickering when getting out of the image area. (http://wordpress.org/support/topic/lazyload-conflicts-more)
* Added compatibility for LazyLoad (unveil.js)
* Added compatibility with Portfolio images for Fluxus theme

= 1.1.3 =
* Fix: https://wordpress.org/support/topic/problem-when-resize-window (regenerate the zoomed image when the page is resized)
* Fix: https://wordpress.org/support/topic/not-working-1307 (when choosing another image from woocommerce gallery, if the image is not big enough to generate a zoom, it was still showing the previously chosen image)

= 1.1.2 =
* Works with MarketPlace plugin

= 1.1.1 =
* Works with Avia Gallery widget

= 1.1.0 =
* Initial commit

== Upgrade Notice ==

Nothing at the moment
