/**
 * Created by kylemobilia on 10.05.17.
 */
function changeSelect(element)
{
    getProductsByCategoryId($(element).val());
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
    $('button.btn-load-more').css('visibility','visible');
}
function getProductsByCategoryId(cat_id)
{
    var data = {
        'action': 'get_products_category_id',
        'cat_id': cat_id
    };
    jQuery.post(ajax_object.ajax_url, data, function(response) {
        var obj = jQuery.parseJSON(response);
        $('section.products').html(obj.html);
        if(obj.enable_button == false)
        {
            $('button.btn-load-more').css('visibility','hidden');
        }else{
            $('button.btn-load-more').css('visibility','visible');
        }
        console.log('change catégorie');
    });

    // Permet de modifier l'url avec la catégorie séléctionné
    var id_param = cat_id.split("-");
    var url_origin = window.location.href.split('?');
    var url_param = '?catergory='+id_param[0];

    if(id_param == 0) {
        var url = url_origin[0];
    }else{
        var url = url_origin[0]+url_param;
    }

    window.location.replace(url);
}

function loadMoreProducts()
{

    var counter = 1;
    jQuery('article.hidden-products').each(function () {
        if (counter <= 12) {
            jQuery(this).removeClass('hidden-products');
            counter++;
        }
    });

    if ( jQuery('article.hidden-products').length <= 0 ) {
        $('button.btn-load-more').css('visibility','hidden');
    }

}

