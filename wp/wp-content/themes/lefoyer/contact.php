<?php
/**
 * Template name: TPL CONTACT
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 04.05.17
 * Time: 02:37
 *
 */

// Data for modify template (add or not under navigation)
define('HOME',false);
define('SHOWROOM',false);
define('NBR_PRODUCTS',0);

require_once 'controllers/base_timber.php';
require_once 'controllers/page_parameter_acf.php';
require_once 'controllers/breadcrumb.php';

$context['image_1'] = get_field('image_1', $post->ID);
$context['image_2'] = get_field('image_2', $post->ID);

$templates = array( 'contact.html.twig' );

Timber::render( $templates, $context );