<?php
/**
 * Template name: TPL SHOWROOM
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 09.05.17
 * Time: 21:49
 */

// Data for modify template (add or not under navigation)
define('HOME',false);
define('SHOWROOM',true);
define('NBR_PRODUCTS',-1);

if(isset($_GET['catergory']) && !empty($_GET['catergory']))
{
    $cat_ID = $_GET['catergory'];
    $_SESSION['term_id'] = $cat_ID;
}else{
    $_SESSION['term_id'] = 0;
}

require_once 'controllers/base_timber.php';
require_once 'controllers/page_parameter_acf.php';
require_once 'controllers/breadcrumb.php';
require_once 'controllers/products.php';
require_once 'controllers/product_categories.php';

if(isset($_GET['catergory']) && !empty($_GET['catergory']))
{
    $context['current_term'] = $_GET['catergory'];
}else{
    $context['current_term'] = 0;
}

$templates = array( 'templates/showroom.html.twig' );

Timber::render( $templates, $context );