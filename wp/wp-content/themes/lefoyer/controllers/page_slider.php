<?php
/**
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 10.05.17
 * Time: 15:24
 *
 */

$slider_page = get_field('slider_page', $post->ID);

$context['show_page_slider'] = (!empty($slider_page)) ? true : false;
$context['page_slider'] = $slider_page;
