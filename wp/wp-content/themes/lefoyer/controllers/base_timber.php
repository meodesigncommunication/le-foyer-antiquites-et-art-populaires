<?php
/**
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 04.05.17
 * Time: 03:52
 *
 */

global $post, $options;

require_once get_template_directory().'/class/Mobile_Detect.php';

define('DEVISE','CHF');
define('AUTHOR_WEBSITE','MEO design & communcation');
define('NAME_NAVIGATION','main');
define('GOOGLE_MAP_API', 'AIzaSyDgbOqncMcK4hytBGFqJoELjllOfS1pAg8');

if ( ! class_exists( 'Timber' ) ) {
    echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
    return;
}

$infos = array();
$detect = new Mobile_Detect();
$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['posts'] = Timber::get_posts();

$context['lang'] = 'fr';
$context['home'] = HOME;
$context['year'] = date('Y');
$context['devise'] = DEVISE;
$context['showroom'] = SHOWROOM;
$context['autor_website'] = AUTHOR_WEBSITE;
$context['options'] = wp_load_alloptions();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['menus'] = wp_get_nav_menu_items(NAME_NAVIGATION);
$context['menus_products_page'] = wp_get_nav_menu_items('products_page');
$context['menus_foyer'] = wp_get_nav_menu_items('foyer');
$context['is_mobile'] = ($detect->isMobile() || $detect->isTablet()) ? true : false;

$context['meta_title'] = get_field('meta_title', $post->ID) ;
$context['meta_description'] = get_field('meta_description', $post->ID) ;
$context['meta_keywords'] = get_field('meta_keywords', $post->ID) ;

/* GET FOOTER DATA */
$infos['shop_name'] = get_field('shop_name','option');
$infos['shop_address'] = get_field('address','option');
$infos['shop_zip'] = get_field('zip','option');
$infos['shop_city'] = get_field('shop_city','option');
$infos['shop_telephone'] = get_field('telephone','option');
$infos['shop_mobile'] = get_field('mobile','option');
$infos['shop_fax'] = get_field('fax','option');
$infos['shop_email'] = get_field('email','option');
$infos['shop_schedule'] = get_field('shop_schedule','option');
$infos['shop_url_facebook'] = get_field('url_facebook','option');

$context['infos'] = $infos;
$context['google_map_api'] = GOOGLE_MAP_API;