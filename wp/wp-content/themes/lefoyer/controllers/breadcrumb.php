<?php
/**
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 09.05.17
 * Time: 22:26
 */

$index_breadcrumb = 0;
$breadcrumb = array();
$parent_post_id = (!empty($post->post_parent)) ? $post->post_parent : 0;

$breadcrumb[$index_breadcrumb]['title'] = $post->post_title;
$breadcrumb[$index_breadcrumb]['url'] = $context['options']['home'].'/'.$post->post_name;

if(!empty($parent_post_id))
{
    $index_breadcrumb++;

    $parent_post = get_post($parent_post_id);

    $breadcrumb[$index_breadcrumb]['title'] = $parent_post->post_title;
    $breadcrumb[$index_breadcrumb]['url'] = $context['options']['home'].'/'.$parent_post->post_name;

    do{
        $index_breadcrumb++;

        $parent_post_parent_id = (!empty($parent_post->post_parent)) ? $parent_post->post_parent : 0;

        if(!empty($parent_post_parent_id))
        {
            $parent_post = get_post($parent_post_parent_id);

            $breadcrumb[$index_breadcrumb]['title'] = $parent_post->post_title;
            $breadcrumb[$index_breadcrumb]['url'] = $context['options']['home'].'/'.$parent_post->post_name;
        }

    }while(!empty($parent_post_parent_id));
}

$context['breadcrumb'] = $breadcrumb;