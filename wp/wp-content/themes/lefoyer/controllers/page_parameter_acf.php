<?php
/**
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 04.05.17
 * Time: 03:44
 *
 */

// Get Page parameter
$blocks = get_field('blocks', $post->ID);
$favorite_list = get_field('favorite', $post->ID);
$event_list = get_field('event', $post->ID);
$other_contents = get_field('other_content', $post->ID);
$context['other_contents'] = $other_contents;

// Get random in list
$max_favorite = count($favorite_list)-1;
$max_event = count($event_list)-1;
$favorite = $favorite_list[rand(0,$max_favorite)];
$event = $event_list[rand(0,$max_event)];

// Get Block to show

$context['show_favorite'] = false;
$context['show_event'] = false;

if(!empty($blocks)){
        foreach($blocks as $block)
        {
            if($block == 'favorite')
            {
                $context['show_favorite'] = true;
            }
            if($block == 'event')
            {
                $context['show_event'] = true;
            }
        }
}

// Get Favorite product page
if($context['show_favorite'])
{
    $favorite_product = get_post($favorite);
    $favorite_product->origine = get_field('origine',$favorite_product->ID);
    $favorite_product->price = money_format_change(get_field('price',$favorite_product->ID), DEVISE);
    $favorite_product->image_featured_url = get_the_post_thumbnail_url($favorite_product);
    $context['favorite_product'] = $favorite_product;
}

// Get Event page
if($context['show_event'])
{
    $event_post = get_post($event);
    $event_post->event_date = get_field('event_date',$event_post->ID);
    $event_post->event_date_end = get_field('event_date_end',$event_post->ID);
    $event_post->event_place = get_field('event_place',$event_post->ID);
    $event_post->txt_intro = get_field('txt_intro',$event_post->ID);
    $event_post->image_featured_url = get_the_post_thumbnail_url($event_post);
    $context['event_post'] = $event_post;
}