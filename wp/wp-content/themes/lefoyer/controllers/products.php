<?php
/**
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 04.05.17
 * Time: 03:44
 *
 */

// Get Products

$id_exclude = (isset($id_exclude) && !empty($id_exclude)) ? $id_exclude : array();

if(isset($cat_ID) && !empty($cat_ID))
{
    $args = array(
        'numberposts'       => NBR_PRODUCTS,
        'post_type'         => 'product',
        'orderby'           => 'date',
        'order'             => 'DESC',
        'post__not_in'      => $id_exclude,
        'meta_query' => array(
            array(
                'key' => 'status',
                'value' => 'available',
            )
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'product_category',
                'terms' => $cat_ID,
                'field' => 'term_id'
            )
        )
    );
}else{
    $args = array(
        'numberposts'       => NBR_PRODUCTS,
        'post_type'         => 'product',
        'orderby'           => 'date',
        'order'             => 'DESC',
        'post__not_in'      => $id_exclude,
        'meta_query' => array(
            array(
                'key' => 'status',
                'value' => 'available',
            )
        )
    );
}

$products = getProducts($args);

//$_SESSION['products'] = $products;
//$_SESSION['index_last_product'] = 11;

$context['count_product'] = count($products);
$context['max_index_product_list'] = MAX_PRODUCTS_LOADED;
$context['products'] = $products;