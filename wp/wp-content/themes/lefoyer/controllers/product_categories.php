<?php
/**
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 09.05.17
 * Time: 23:34
 */

$args = array(
    'taxonomy' => 'product_category'
);

$terms = get_terms($args);

$context['terms'] = $terms;
$context['all_products_count'] = wp_count_posts('product');

