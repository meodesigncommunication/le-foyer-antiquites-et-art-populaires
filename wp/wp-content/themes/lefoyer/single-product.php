<?php
/**
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 11.05.17
 * Time: 14:49
 *
 */

// Data for modify template (add or not under navigation)
define('HOME',false);
define('SHOWROOM',false);
define('NBR_PRODUCTS',4);

require_once 'controllers/base_timber.php';
require_once 'controllers/page_parameter_acf.php';

$entitled_date = array(
    'date' => 'Date',
    'around' => 'Vers',
    'age' => 'époque'
);
$age = array(
    'start' => 'Début',
    'middle' => 'Début',
    'end' => 'Début',
    'no_age' => ''
);
$materials = array(
    'wood' => 'Bois',
    'materials' => 'Matériaux'
);
$other_size = array(
    'depth' => 'Profondeur',
    'length' => 'Longueur',
    'diameter' => 'Diamètre'
);

$product = $post;
$dir = wp_get_upload_dir();
$full_path = $dir['basedir'].'/products_pdf/';
$full_path_url = $dir['baseurl'].'/products_pdf/';

$product->reference = get_field('reference',$product->ID);
$product->entitled_date = $entitled_date[get_field('entitled_date',$product->ID)];
$product->age = (get_field('entitled_date',$product->ID) == 'age') ? $age[get_field('age',$product->ID)].' ' : '';
$product->date = get_field('date',$product->ID);
$product->origine = get_field('origine',$product->ID);
$product->status = get_field('status',$product->ID);
$product->materials = $materials[get_field('materials',$product->ID)];
$product->material_value = get_field('material_value',$product->ID);
$product->price = money_format_change(get_field('price',$product->ID), DEVISE, false);
$product->measuring_unit = get_field('measuring_unit',$product->ID);
$product->height = get_field('height',$product->ID);
$product->width = get_field('width',$product->ID);
$product->other_size = $other_size[get_field('other_size',$product->ID)];
$product->other_size_value = get_field('other_size_value',$product->ID);
$product->size_comment = get_field('size_comment',$product->ID);
$product->restoration = get_field('restoration',$product->ID);
$product->restoration_wood = (get_field('restoration_wood',$product->ID)) ? get_field('restoration_wood',$product->ID) : '-';
$product->restoration_paint = (get_field('restoration_paint',$product->ID)) ? get_field('restoration_paint',$product->ID) : '-';
$product->image_featured_url = get_the_post_thumbnail_url($product);
$product->gallery = get_field('products_gallery',$product->ID);

$meta_title_product = get_field('meta_title', $product->ID);
$meta_description_product = get_field('meta_description', $product->ID);
$meta_keywords_product = get_field('meta_keywords', $product->ID);

$context['meta_title'] = (!empty($meta_title_product)) ? get_field('meta_title', $product->ID) : $product->post_title ;
$context['meta_description'] = (!empty($meta_description_product)) ? get_field('meta_description', $product->ID) : $product->post_content ;
$context['meta_keywords'] = (!empty($meta_keywords_product)) ? get_field('meta_keywords', $product->ID) : '' ;

if(file_exists($full_path.$product->ID.'-'.$product->post_name.'.pdf')){

    $product->pdf = $full_path_url.$product->ID.'-'.$product->post_name.'.pdf';

}else{

    if(generateProductPDF( $pproduct->ID ))
    {
        $product->pdf = $full_path_url.$product->ID.'-'.$product->post_name.'.pdf';
    }else{
        $product->pdf = '';
    }

}

$terms = wp_get_post_terms( $post->ID, 'product_category');

$product->term = $terms[0];
$product->term->reference = get_field('term_reference', 'term_'.$product->term->term_id);

$context['product'] = $product;

$cat_ID = $product->term->term_id;

$id_exclude = array($product->ID);

require_once 'controllers/products.php';

$breadcrumb[0]['title'] = $post->post_title;
$breadcrumb[0]['url'] = $context['options']['home'].'/products/'.$post->post_name;

$breadcrumb[1]['title'] = $terms[0]->name;
$breadcrumb[1]['url'] = $context['options']['home'].'/showroom/?catergory='.$terms[0]->term_id;

foreach ($terms as $term){
    if(isset($_SESSION['term_id']) && $term->term_id == $_SESSION['term_id'])
    {
        $breadcrumb[1]['title'] = $term->name;
        $breadcrumb[1]['url'] = $context['options']['home'].'/showroom/?catergory='.$term->term_id;
    }
}

$breadcrumb[2]['title'] = 'showroom';
$breadcrumb[2]['url'] = $context['options']['home'].'/showroom/';

$breadcrumb[3]['title'] = 'Accueil';
$breadcrumb[3]['url'] = $context['options']['home'];

$context['breadcrumb'] = $breadcrumb;

$templates = array( 'templates/product.html.twig' );

if($product->status == 'sold_out'){
    header('location: http://www.antiquiteslefoyer.ch/showroom');
}

Timber::render( $templates, $context );