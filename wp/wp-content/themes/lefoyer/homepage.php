<?php
/**
 * Template name: TPL HOMEPAGE
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 04.05.17
 * Time: 02:37
 *
 */

// Data for modify template (add or not under navigation)
define('HOME',true);
define('SHOWROOM',false);
define('NBR_PRODUCTS',4);

require_once 'controllers/base_timber.php';
require_once 'controllers/page_parameter_acf.php';
require_once 'controllers/products.php';

$count_slider = 0;
$slider = array();
$products = array();
$templates = array( 'templates/homepage.html.twig' );

// Get homepage slider
// check if the repeater field has rows of data
if( have_rows('slider') ):
    // loop through the rows of data
    while ( have_rows('slider') ) : the_row();
        // get sub field value
        $slider[$count_slider]['image'] = get_sub_field('home_slide_image');
        $slider[$count_slider]['text'] = get_sub_field('home_slide_text');
        $slider[$count_slider]['align_horizontal'] = get_sub_field('home_slide_alignement_horizontal');
        $slider[$count_slider]['align_vertical'] = get_sub_field('home_slide_alignement_vertical');
        $count_slider++;
    endwhile;
endif;
$context['home_slider'] = $slider;

Timber::render( $templates, $context );