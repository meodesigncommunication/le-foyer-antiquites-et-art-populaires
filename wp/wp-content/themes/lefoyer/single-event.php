<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

// Data for modify template (add or not under navigation)
define('HOME',false);
define('SHOWROOM',false);
define('NBR_PRODUCTS',0);

require_once 'controllers/base_timber.php';
require_once 'controllers/page_parameter_acf.php';
require_once 'controllers/breadcrumb.php';
require_once 'controllers/page_slider.php';

$breadcrumb[1]['title'] = 'Accueil';
$breadcrumb[1]['url'] = $context['options']['home'];

$context['breadcrumb'] = $breadcrumb;
$context['featured_image'] = get_the_post_thumbnail_url($post);

$templates = array( 'event.html.twig' );

Timber::render( $templates, $context );