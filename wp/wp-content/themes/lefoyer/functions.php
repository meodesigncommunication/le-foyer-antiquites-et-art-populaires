<?php

require_once 'class/fpdf.php';
require_once 'class/fpdi.php';

if(session_id() == ''){
    session_start();
}

function add_js_scripts() {
    wp_register_script( 'showroom_js',  '/wp-content/themes/lefoyer/js/showroom.js', false, '1.0', true );
    wp_enqueue_script( 'showroom_js' );

    wp_localize_script( 'showroom_js', 'ajax_object', array('ajax_url' => admin_url( 'admin-ajax.php' )) );
}
add_action('wp_enqueue_scripts', 'add_js_scripts');


if ( ! class_exists( 'Timber' ) ) {
    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
    } );
    return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

    function __construct() {
        add_theme_support( 'post-formats' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'menus' );
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
        parent::__construct();
    }

    function register_post_types() {
        //this is where you can register custom post types
    }

    function register_taxonomies() {
        //this is where you can register custom taxonomies
    }

    function add_to_context( $context ) {
        $context['foo'] = 'bar';
        $context['stuff'] = 'I am a value set in your functions.php file';
        $context['notes'] = 'These values are available everytime you call Timber::get_context();';
        $context['menu'] = new TimberMenu();
        $context['site'] = $this;
        $context['url_plugin'] = plugins_url();
        return $context;
    }

    function myfoo( $text ) {
        $text .= ' bar!';
        return $text;
    }

    function add_to_twig( $twig ) {
        /* this is where you can add your own fuctions to twig */
        $twig->addExtension( new Twig_Extension_StringLoader() );
        $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
        return $twig;
    }

}

new StarterSite();

show_admin_bar( false );

/*
 *  CUSTOM POST TYPE PRODUCT
 *  CUSTOM TAXONOMY
 */
add_action( 'init', 'create_post_type_product' );
function create_post_type_product() {
    register_post_type( 'product',
        array(
            'labels' => array(
                'name' => __( 'Produits' ),
                'singular_name' => __( 'Product' )
            ),
            'menu_icon' => 'dashicons-cart',
            'public' => true,
            'rewrite' => array('slug' => 'products'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            )
        )
    );
    register_taxonomy(
        'product_category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'product',        //post type name
        array(
            'hierarchical' => true,
            'show_in_admin_bar' => true,
            'label' => 'Catégories de produit',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'product_category', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
}

add_action( 'init', 'create_post_type_event' );
function create_post_type_event() {
    register_post_type( 'event',
        array(
            'labels' => array(
                'name' => __( 'Événements' ),
                'singular_name' => __( 'Events' )
            ),
            'menu_icon' => 'dashicons-calendar-alt',
            'public' => true,
            'rewrite' => array('slug' => 'events'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            )
        )
    );
}

add_action('acf/init', 'my_acf_init');
function my_acf_init() {
    if( function_exists('acf_add_options_page') ) {
        $option_page = acf_add_options_page(array(
            'page_title' 	=> 'Infos Magasin',
            'menu_title' 	=> 'Magasin',
            'menu_slug' 	=> 'shop-info-settings',
            'menu_icon' => 'dashicons-info',
            'capability' 	=> 'edit_posts',
            'redirect' 	=> false
        ));
    }
}

/*
 *  Return money with Swiss Format
 *  @number
 *  @devise
 */
function money_format_change($number,$devise,$span=true)
{
    $number =  number_format($number, 2,".","'");
    $number = explode('.',$number);

    if(empty($number[0])) {

        return 'Prix sur demande';

    }else{

        if((empty($number[1]) || $number[1] == '00'))
        {
            return ($span) ? '<span class="label">'.$devise.'</span> '.$number[0].'.-' : $devise.' '.$number[0].'.-';
        }

        return ($span) ? '<span class="label">'.$devise.'</span> '.$number[0].'.'.$number[1] : $devise.' '.$number[0].'.'.$number[1];

    }


}

function getProducts($args)
{
    $products = array();
    $products_list = get_posts($args);

    $entitled_date = array(
        'date' => 'Date',
        'around' => 'Vers',
        'age' => 'époque'
    );
    $age = array(
        'start' => 'Début',
        'middle' => 'Début',
        'end' => 'Début',
        'no_age' => ''
    );
    $materials = array(
        'wood' => 'Bois',
        'materials' => 'Matériaux'
    );
    $other_size = array(
        'depth' => 'Profondeur',
        'length' => 'Longueur',
        'diameter' => 'Diamètre'
    );

    foreach($products_list as $product)
    {
        $product->status = get_field('status',$product->ID);
        if($product->status == 'available')
        {
            $attachement_id = get_post_thumbnail_id($product);
            $attachement_small = wp_get_attachment_image_src($attachement_id,'thumbnail');
            $attachement_medium = wp_get_attachment_image_src($attachement_id,'medium');

            $product->reference = get_field('reference',$product->ID);

            $product->entitled_date = get_field('entitled_date',$product->ID);
            $product->entitled_date_label = $entitled_date[get_field('entitled_date',$product->ID)];

            $product->age = $age[get_field('age',$product->ID)];

            $product->date = get_field('date',$product->ID);
            $product->origine = get_field('origine',$product->ID);
            $product->materials = $materials[get_field('materials',$product->ID)];
            $product->material_value = get_field('material_value',$product->ID);
            $product->price = money_format_change(get_field('price',$product->ID), DEVISE, true);
            $product->measuring_unit = get_field('measuring_unit',$product->ID);
            $product->height = get_field('height',$product->ID);
            $product->width = get_field('width',$product->ID);

            $product->other_size = $other_size[get_field('other_size',$product->ID)];

            $product->other_size_value = get_field('other_size_value',$product->ID);
            $product->size_comment = get_field('size_comment',$product->ID);
            $product->restoration = get_field('restoration',$product->ID);
            $product->restoration_wood = get_field('restoration_wood',$product->ID);
            $product->restoration_paint = get_field('restoration_paint',$product->ID);
            $product->image_featured_url_small = $attachement_small[0];
            $product->image_featured_url_medium = $attachement_medium[0];
            $product->image_featured_url = get_the_post_thumbnail_url($product);
            $product->gallery = get_field('products_gallery',$product->ID);
            $products[] = $product;
        }
    }

    return $products;
}

require_once get_template_directory().'/ajax/ajax-products-action.php';


// Search on custom fields
function custom_search_join ($join){
    global $pagenow, $wpdb;
    $types = ['product'];
    // I want the filter only when performing a search on edit page of Custom Post Type in $types array
    if ( is_admin() && $pagenow=='edit.php' && in_array( $_GET['post_type'], $types ) && isset( $_GET['s'] ) ) {
        $join .='LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    return $join;
}
add_filter('posts_join', 'custom_search_join' );
function custom_search_where( $where ){
    global $pagenow, $wpdb;
    $types = ['product'];
    // I want the filter only when performing a search on edit page of Custom Post Type in $types array
    if ( is_admin() && $pagenow=='edit.php' && in_array( $_GET['post_type'], $types ) && isset( $_GET['s'] ) ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }
    return $where;
}
add_filter( 'posts_where', 'custom_search_where' );
function custom_search_distinct( $where ){
    global $pagenow, $wpdb;
    $types = ['product'];
    if ( is_admin() && $pagenow=='edit.php' && in_array( $_GET['post_type'], $types ) && isset( $_GET['s'] ) ) {
        return "DISTINCT";

    }
    return $where;
}
add_filter( 'posts_distinct', 'custom_search_distinct' );

// Ajout d'une colonne ACF
add_filter( 'manage_product_posts_columns', 'set_custom_edit_produits_columns' );
add_action( 'manage_product_posts_custom_column' , 'custom_produits_column', 10, 2 );
function set_custom_edit_produits_columns($columns) {
    $columns['reference'] = __( 'Reference' );
    return $columns;
}
function custom_produits_column( $column, $post_id ) {
    switch ( $column ) {

        case 'reference' :
            $terms = get_field( 'reference', $post_id );
            if ( is_string( $terms ) )
                echo $terms;
            break;
    }
}

/*
 *  Generate Certificat
 */
function generate_product_certificat($post_id){

    $post_type = get_post_type($post_id);

    if($post_type == 'product' && !empty($post_id)){
        generateProductPDF( $post_id );
    }

}
add_action( 'save_post', 'generate_product_certificat' );


/*
 *  Add Element in admin
 */
/*function my_custom_admin_head() {
    echo '  <style>
                #pdf_generate{
                    color: #FFF;
                    background-color: #cd0000;
                    border: 1px solid #cd0000;
                    bottom: 10px;
                    padding: 10px 15px;
                    position: fixed;
                    right: 10px;
                    z-index: 200;
                    cursor: pointer;
                }
                #pdf_generate:hover{
                    background-color: #b30000;
                    border: 1px solid #b30000;
                }
            </style>';
}
function button_generate_certificat() {
    echo '<div id="pdf_generate" title="Génére les certificats pour les produits">Générer les certificats</div>';
}
function my_custom_footer_admin()
{

    echo '  <script type="text/javascript">

                jQuery("#pdf_generate").click(function(){

                    alert("CLICK CLICK");

                     jQuery.post(
                        ajaxurl,
                        {
                            "action": "get_create_all_products_pdf",
                            "id": 0
                        },
                        function(response){
                            console.log(response);
                        }
                    );
                });

            </script>
         ';
}
add_action( 'admin_head', 'my_custom_admin_head' );
add_action( 'admin_footer', 'my_custom_footer_admin' );
add_action( 'in_admin_header', 'button_generate_certificat' );*/