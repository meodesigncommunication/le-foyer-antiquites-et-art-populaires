<?php
/**
 *
 * Author: MEO design & communication (Kyle Mobilia)
 * Date: 04.05.17
 * Time: 04:33
 *
 */


/*
 *  Return JSON of product by category id
 */
function getInitProductsByCategoryId()
{
    $html = '';
    $cat_ID = $_POST['cat_id'];

    $_SESSION['term_id'] = $cat_ID;

    // Get Products
    if(!empty($cat_ID))
    {
        $args = array(
            'numberposts'       => -1,
            'post_type'         => 'product',
            'orderby'           => 'date',
            'order'             => 'DESC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_category',
                    'terms' => $cat_ID,
                    'field' => 'term_id'
                )
            )
        );
    }else{
        $args = array(
            'numberposts'       => -1,
            'post_type'         => 'product',
            'orderby'           => 'date',
            'order'             => 'DESC'
        );
    }

    $products = getProducts($args);

    $_SESSION['products'] = $products;
    $max_product = MAX_PRODUCTS_LOADED;

    for ($i = 0; $i < $max_product ; $i++)
    {
        $product = $products[$i];

        if(!empty($product)){
            $html .= '<article product-id="'.$product->ID.'">';

            if(!empty($product->image_featured_url_medium))
            {
                $html .= '<a href="'.$product->guid.'" title="'.$product->post_title.'"><img src="'.$product->image_featured_url_medium.'" alt=""></a>';
            }else{
                $html .= '<a href="'.$product->guid.'" title="'.$product->post_title.'"><img src="'.get_template_directory_uri().'/../uploads/2017/05/No-image.png" alt=""></a>';
            }

            $html .=    '<a class="product_title" href="'.$product->guid.'" title="'.$product->post_title.'">'.$product->post_title.'</a>';

            if($product->entitled_date == 'age')
            {
                $html .=    '<p><span class="label">'.$product->entitled_date_label.'</span> '.$product->age.' '.$product->date.'</p>';
            }else{
                $html .=    '<p><span class="label">'.$product->entitled_date_label.'</span> '.$product->date.'</p>';
            }

            $html .=    '<p><span class="label">ORIGINE</span> '.$product->origine.'</p>';
            $html .=    '<p class="price">'.money_format_change(get_field('price',$product->ID), 'CHF', true).'</p>';
            $html .=    '<a class="link" href="'.$product->guid.'" title="'.$product->post_title.'">Voir en détail</a>';
            $html .= '</article>';
        }
    }

    $_SESSION['index_last_product'] = $i;

    $json['html'] = $html;
    $json['enable_button'] = (count($products) <= $i) ? false : true;

    echo json_encode($json);

    die();
}
add_action( 'wp_ajax_get_products_category_id', 'getInitProductsByCategoryId' );
add_action( 'wp_ajax_nopriv_get_products_category_id', 'getInitProductsByCategoryId' );

function getMoreProducts()
{
    $html = '';
    $json = array();

    $products = $_SESSION['products'];
    $max_product = $_SESSION['index_last_product']+MAX_PRODUCTS_LOADED;


   /* echo 'INDEX PRODUCT = '.$_SESSION['index_last_product'].'<br/>';

    echo 'MAX PRODUCT = '.$max_product.'<br/>';

    echo '<pre>';
    print_r($products);
    echo '</pre>';

    exit();*/

    for ($i = $_SESSION['index_last_product']; $i < $max_product ; $i++)
    {
        $product = $products[$i];

        if(!empty($product)){
            $html .= '<article product-id="'.$product->ID.'">';

            if(!empty($product->image_featured_url_medium))
            {
                $html .= '<a href="'.$product->guid.'" title="'.$product->post_title.'"><img src="'.$product->image_featured_url_medium.'" alt=""></a>';
            }else{
                $html .= '<a href="'.$product->guid.'" title="'.$product->post_title.'"><img src="'.get_template_directory_uri().'/../uploads/2017/05/No-image.png" alt=""></a>';
            }

            $html .=    '<a class="product_title" href="'.$product->guid.'" title="'.$product->post_title.'">'.$product->post_title.'</a>';

            if($product->entitled_date == 'age')
            {
                $html .=    '<p><span class="label">'.$product->entitled_date_label.'</span> '.$product->age.' '.$product->date.'</p>';
            }else{
                $html .=    '<p><span class="label">'.$product->entitled_date_label.'</span> '.$product->date.'</p>';
            }

            $html .=    '<p><span class="label">ORIGINE</span> '.$product->origine.'</p>';
            $html .=    '<p class="price">'.money_format_change(get_field('price',$product->ID), 'CHF', true).'</p>';
            $html .=    '<a class="link" href="'.$product->guid.'" title="'.$product->post_title.'">Voir en détail</a>';
            $html .= '</article>';
        }
    }

    $_SESSION['index_last_product'] = $i;

    $json['html'] = $html;
    $json['enable_button'] = (count($products) <= $i) ? false : true;

    echo json_encode($json);

    die();
}
add_action( 'wp_ajax_get_more_products', 'getMoreProducts' );
add_action( 'wp_ajax_nopriv_get_more_products', 'getMoreProducts' );

/*
 *  CREATE PDF PRODUCT
 */
function generateProductPDF( $post_id = 0 ) {

    $post = get_post($post_id);

    if($post->post_type == 'product' && !empty($post))
    {
        $pdf = new FPDI();
        $post_id = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : $post_id;

        $entitled_date = array(
            'date' => 'DATE',
            'around' => 'VERS',
            'age' => 'ÉPOQUE'
        );
        $age = array(
            'start' => 'Début',
            'middle' => 'Milieu',
            'end' => 'Fin'
        );
        $materials = array(
            'wood' => 'BOIS',
            'materials' => 'MATÉRIAUX'
        );
        $other_size = array(
            'no_other_size' => '',
            'depth' => 'Profondeur',
            'length' => 'Longueur',
            'diameter' => 'Diamètre'
        );

        define('FPDF_FONTPATH', get_template_directory()."/fonts/");

        // Get Product
        $product = get_post($post_id);
        $product->reference = get_field('reference',$product->ID);
        $product->entitled_date = $entitled_date[get_field('entitled_date',$product->ID)];
        $product->age = (get_field('age',$product->ID) != 'no_age') ? $age[get_field('age',$product->ID)].' ' : '';
        $product->date = get_field('date',$product->ID);
        $product->origine = get_field('origine',$product->ID);
        $product->materials = $materials[get_field('materials',$product->ID)];
        $product->material_value = get_field('material_value',$product->ID);
        $product->price = money_format_change(get_field('price',$product->ID), 'CHF', false);
        $product->measuring_unit = get_field('measuring_unit',$product->ID);
        $product->height = get_field('height',$product->ID);
        $product->width = get_field('width',$product->ID);
        $product->other_size = $other_size[get_field('other_size',$product->ID)];
        $product->other_size_value = get_field('other_size_value',$product->ID);
        $product->size_comment = get_field('size_comment',$product->ID);
        $product->restoration = get_field('restoration',$product->ID);
        $product->restoration_wood = (get_field('restoration_wood',$product->ID)) ? get_field('restoration_wood',$product->ID) : '-';
        $product->restoration_paint = (get_field('restoration_paint',$product->ID)) ? get_field('restoration_paint',$product->ID) : '-';
        $product->image_featured_url = get_the_post_thumbnail_url($product);

        // Start create PDF

        $pagecount = $pdf->setSourceFile(get_template_directory().'/pdf_template/tpl_product.pdf');
        $tplidx = $pdf->importPage(1);

        $pdf->addPage();

        $pdf->AddFont('Gilda','','GildaDisplay-Regular.php');
        $pdf->AddFont('OpenSans','B','OpenSans-Bold.php');
        $pdf->AddFont('Abril_Fatface','','AbrilFatface-Regular.php');

        $pdf->SetFont('Gilda','',18);
        $pdf->SetTextColor(255,255,255);
        $pdf->useTemplate($tplidx, 0, 0);

        /**************************************/

        // Print Title PDF
        $pdf->SetX(0);
        $pdf->SetY(24);
        $pdf->Cell(0,0,utf8_decode($product->post_title),0,0,'C');

        // Print Image PDF
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->image($product->image_featured_url, 10, $y+22, 90, 90);

        // Print Detail product title and product description PDF
        $pdf->SetX(100);
        $pdf->SetY($y+25);
        $pdf->SetLeftMargin(109);
        $pdf->SetTextColor(207,0,15);
        $pdf->SetFont('Gilda','',16);
        $pdf->Cell(0,0,utf8_decode('Détail du produit'),0,0,'L');

        $pdf->SetY($pdf->GetY()+5);
        $y = $pdf->GetY();
        $pdf->SetFont('Gilda','',9);
        $pdf->write(6,utf8_decode($product->post_content));

        // --------------------------------------------------------------------

        // Print Product reference PDF
        $pdf->SetY($y+47);
        $pdf->SetFont('OpenSans','B',6);
        $pdf->Cell(20,0,utf8_decode('RÉFÉRENCE'),0,0,'L');
        $pdf->SetY($y+52);
        $pdf->SetFont('Gilda','',9);
        $pdf->Cell(20,0,utf8_decode($product->reference),0,0,'L');

        // Print Product origine PDF
        $y = $pdf->GetY();
        $x = $pdf->GetX();
        $pdf->SetY($y-5);
        $pdf->SetX($x+30);
        $pdf->SetFont('OpenSans','B',6);
        $pdf->Cell(20,0,utf8_decode('ORIGINE'),0,0,'L');
        $pdf->SetY($y);
        $pdf->SetX($x+30);
        $pdf->SetFont('Gilda','',9);
        $pdf->Cell(20,0,utf8_decode($product->origine),0,0,'L');

        // --------------------------------------------------------------------

        // Print Product Date PDF
        $pdf->SetY($y+11);
        $y = $pdf->GetY();
        $pdf->SetFont('OpenSans','B',6);
        $pdf->Cell(20,0,utf8_decode(strtoupper($product->entitled_date)),0,0,'L');

        $pdf->SetY($y+5);
        $pdf->SetFont('Gilda','',9);
        if(!empty($product->age)){
            $pdf->Cell(20,0,utf8_decode($product->age.' '.$product->date),0,0,'L');
        }else{
            $pdf->Cell(20,0,utf8_decode($product->date),0,0,'L');
        }

        // Print Product materieaux PDF
        $y = $pdf->GetY();
        $x = $pdf->GetX();

        $pdf->SetY($y-5);
        $pdf->SetX($x+30);
        $pdf->SetFont('OpenSans','B',6);
        $pdf->Cell(20,0,utf8_decode($product->materials),0,0,'L');

        $pdf->SetY($y);
        $pdf->SetX($x+30);
        $pdf->SetFont('Gilda','',9);
        $pdf->Cell(20,0,utf8_decode($product->material_value),0,0,'L');

        // --------------------------------------------------------------------

        // Print Product dimmension PDF
        $y = $pdf->GetY();
        $pdf->SetY($y+11);
        $y = $pdf->GetY();
        $x = $pdf->GetX();
        $pdf->SetFont('OpenSans','B',6);
        $pdf->Cell(20,0,utf8_decode('DIMENSIONS'),0,0,'L');

        $pdf->SetFont('Gilda','',9);
        $pdf->SetY($y+5);
        $pdf->Cell(20,0,utf8_decode('Hauteur:'),0,0,'L');
        $pdf->SetX($x+30);
        $pdf->Cell(20,0,utf8_decode($product->height.' '.$product->measuring_unit),0,0,'L');

        $pdf->SetY($y+10);
        $pdf->SetX($x);
        $pdf->Cell(20,0,utf8_decode('Largeur:'),0,0,'L');
        $pdf->SetX($x+30);
        $pdf->Cell(20,0,utf8_decode($product->width.' '.$product->measuring_unit),0,0,'L');

        $pdf->SetY($y+15);
        $pdf->SetX($x);

        if(!empty($product->other_size))
        {
            $pdf->Cell(20,0,utf8_decode($product->other_size.':'),0,0,'L');
            $pdf->SetX($x+30);
            $pdf->Cell(20,0,utf8_decode($product->other_size_value.' '.$product->measuring_unit),0,0,'L');
        }

        // --------------------------------------------------------------------

        // Print Product dimmension PDF
        $y = $pdf->GetY();
        $pdf->SetY($y+11);
        $y = $pdf->GetY();
        $x = $pdf->GetX();
        $pdf->SetFont('OpenSans','B',6);
        $pdf->Cell(20,0,utf8_decode('RESTAURATION'),0,0,'L');

        $pdf->SetFont('Gilda','',9);
        $pdf->SetY($y+5);
        $pdf->Cell(20,0,utf8_decode('Bois:'),0,0,'L');
        $pdf->SetX($x+30);
        $pdf->Cell(20,0,utf8_decode($product->restoration_wood),0,0,'L');

        $pdf->SetY($y+10);
        $pdf->SetX($x);
        $pdf->Cell(20,0,utf8_decode('Peinture:'),0,0,'L');
        $pdf->SetX($x+30);
        $pdf->Cell(20,0,utf8_decode($product->restoration_paint),0,0,'L');

        // --------------------------------------------------------------------

        // Print Product dimmension PDF
        $y = $pdf->GetY();
        $pdf->SetY($y+11);
        $y = $pdf->GetY();
        $x = $pdf->GetX();
        $pdf->SetFont('OpenSans','B',6);
        $pdf->Cell(20,0,utf8_decode('VALEUR'),0,0,'L');

        $pdf->SetFont('Abril_Fatface','',13);
        $pdf->SetY($y+5);
        $pdf->Cell(20,0,utf8_decode($product->price),0,0,'L');


            /**************************************/

        $dir = wp_get_upload_dir();
        $full_path = $dir['basedir'].'/products_pdf/';

        if($pdf->Output('f',$full_path.$product->ID.'-'.$product->post_name.'.pdf'))
        {
            return true;
        }
    }
    return false;
}
//add_action( 'save_post', 'generateProductPDF' );
add_action( 'wp_ajax_get_generate_product_pdf', 'generateProductPDF' );
add_action( 'wp_ajax_nopriv_get_generate_product_pdf', 'generateProductPDF' );

function createAllProductsPDF()
{

    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'product'
    );

    $products = get_posts($args);

    foreach($products as $product){
        echo 'ID => '.$product->ID.'<br/>';
        generateProductPDF( $product->ID );
    }

    die();

}
add_action( 'wp_ajax_get_create_all_products_pdf', 'createAllProductsPDF' );
add_action( 'wp_ajax_nopriv_get_create_all_products_pdf', 'createAllProductsPDF' );